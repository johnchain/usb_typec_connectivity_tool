# USB_TypeC_Connectivity_Test_Demo_v2

一个串口图形化测试工具，定制化比较强（后续有机会再改为通用型Tool吧。。)

### 运行环境：
* MacOS 12.3+
* XCode 9.3
* Swift 3

### 串口通信结合使用到了：
* [ORSSerial](https://github.com/armadsen/ORSSerialPort) 
* [SwiftSerial](https://github.com/yeokm1/SwiftSerial)

### 日志记录使用：
* [Log4swift](https://github.com/jduquennoy/Log4swift)

### TODO
* UART功能待验证
* 进度条显示