#ifdef __OBJC__
#import <Cocoa/Cocoa.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif


FOUNDATION_EXPORT double Pods_USB_TypeC_Connectivity_Test_Demo_v2VersionNumber;
FOUNDATION_EXPORT const unsigned char Pods_USB_TypeC_Connectivity_Test_Demo_v2VersionString[];

