//
//  ViewController.swift
//  USB_TypeC_Connectivity_Test_Demo_v2
//
//  Created by johnchain on 18/03/2018.
//  Copyright © 2018 J. All rights reserved.
//

import Cocoa
import ORSSerial
import Log4swift

class ViewController: NSViewController {
    
    var statePDSetting:String = "AA"
    var statePortSetting:String = "ABAUTO"
    var gBaudrate: BaudRate = .baud9600
    var gLane: Int = 4
    
    let serialPortManager = ORSSerialPortManager.shared()
    let boardController = SerialBoardController()
    var serialPort: SerialPort!
    var timerProgress:Timer!
    var pdtimer: Timer!
    var testtimer: Timer!

    @IBOutlet weak var messageSend: NSTextFieldCell!
    @IBOutlet weak var serialPortSelection: NSPopUpButton!
    @IBOutlet weak var baudrateSelection: NSPopUpButton!
    @IBOutlet weak var cbline: NSPopUpButton!
    @IBOutlet weak var serialPortConnector: NSButton!
    
    @IBOutlet weak var btnAATest: NSButton!
    @IBOutlet weak var btnABTest: NSButton!
    @IBOutlet weak var btnBATest: NSButton!
    @IBOutlet weak var btnBBTest: NSButton!
    @IBOutlet weak var btnPDTest: NSButton!
    @IBOutlet weak var btnAutoTest: NSButton!
    
    @IBOutlet weak var ckDebug: NSButton!
    @IBOutlet weak var ckName: NSButton!
    @IBOutlet weak var ckA01: NSButton!
    @IBOutlet weak var ckA02: NSButton!
    @IBOutlet weak var ckA03: NSButton!
    @IBOutlet weak var ckA04: NSButton!
    @IBOutlet weak var ckA05: NSButton!
    @IBOutlet weak var ckA06: NSButton!
    @IBOutlet weak var ckA07: NSButton!
    @IBOutlet weak var ckA08: NSButton!
    @IBOutlet weak var ckA09: NSButton!
    @IBOutlet weak var ckA10: NSButton!
    @IBOutlet weak var ckA11: NSButton!
    @IBOutlet weak var ckA12: NSButton!
    
    @IBOutlet weak var ckB01: NSButton!
    @IBOutlet weak var ckB02: NSButton!
    @IBOutlet weak var ckB03: NSButton!
    @IBOutlet weak var ckB04: NSButton!
    @IBOutlet weak var ckB05: NSButton!
    @IBOutlet weak var ckB06: NSButton!
    @IBOutlet weak var ckB07: NSButton!
    @IBOutlet weak var ckB08: NSButton!
    @IBOutlet weak var ckB09: NSButton!
    @IBOutlet weak var ckB10: NSButton!
    @IBOutlet weak var ckB11: NSButton!
    @IBOutlet weak var ckB12: NSButton!
    
    @IBOutlet weak var progressBar: NSProgressIndicator!
    @IBOutlet weak var lTest: NSTextField!
    @IBOutlet weak var leedit: NSTextField!
    @IBOutlet weak var cbitem: NSPopUpButton!
    
    //Setting refs
    @IBOutlet weak var ckPD: NSButton!
    @IBOutlet weak var ckDP: NSButton!
    @IBOutlet weak var ckUSB20: NSButton!
    
    @IBOutlet weak var ckLoop: NSButton!
    @IBOutlet weak var cklogonlyone: NSButton!
    @IBOutlet weak var loopCountStepper: NSStepper!
    @IBOutlet weak var spinLoop: NSTextField!
    
    //Test infos
    @IBOutlet weak var lvbus: NSTextField!
    @IBOutlet weak var lisens: NSTextField!
    @IBOutlet weak var lvcc1: NSTextField!
    @IBOutlet weak var lvcc2: NSTextField!
    @IBOutlet weak var lusb20: NSTextField!
    @IBOutlet weak var ldisplayers: NSTextField!
    @IBOutlet weak var ldpgen: NSTextField!
    @IBOutlet weak var ldplane0: NSTextField!
    @IBOutlet weak var ldplane1: NSTextField!
    @IBOutlet weak var ldplane2: NSTextField!
    @IBOutlet weak var ldplane3: NSTextField!
    @IBOutlet weak var ledidinfo: NSTextField!
    @IBOutlet weak var ledidtime: NSTextField!
    
    @IBOutlet var listResult: NSTextView!
    @IBOutlet var listRecv: NSTextView!
    @IBOutlet weak var txtUartCMD: NSTextField!
    
    @IBAction func fnStepper(_ sender: Any) {
        let stepper:NSStepper = sender as! NSStepper
        spinLoop.stringValue = String(stepper.intValue)
    }
    @IBAction func fnPDSettingChange(_ sender: Any) {
        let radioButton:NSButton = sender as! NSButton
        let title:String = radioButton.title
        switch title {
        case "A-A PD Test":
            statePDSetting = "AA"
        case "A-B PD Test":
            statePDSetting = "AB"
        case "B-A PD Test":
            statePDSetting = "BA"
        case "B-B PD Test":
            statePDSetting = "BB"
        default:
            statePDSetting = "AA"
        }
        print("statePDSetting = \(statePDSetting)")
    }
    @IBAction func fnPortSettingChange(_ sender: Any) {
        let radioButton:NSButton = sender as! NSButton
        let title:String = radioButton.title
        switch title {
        case "A and B Port Auto Test":
            statePortSetting = "ABAUTO"
        case "A Port Auto Test":
            statePortSetting = "AAUTO"
        case "B Port Auto Test":
            statePortSetting = "BAUTO"
        default:
            statePortSetting = "ABAUTO"
        }
        print("statePortSetting = \(statePortSetting)")
    }

    @IBAction func fnBaudrateSelection(_ sender: Any) {
        let popButton: NSPopUpButton = sender as! NSPopUpButton
        let baudrateStr: String = (popButton.selectedItem?.title)!
        switch baudrateStr {
        case "110":
            gBaudrate = .baud110
        case "300":
            gBaudrate = .baud300
        case "600":
            gBaudrate = .baud600
        case "1200":
            gBaudrate = .baud1200
        case "2400":
            gBaudrate = .baud2400
        case "4800":
            gBaudrate = .baud4800
        case "9600":
            gBaudrate = .baud9600
        case "19200":
            gBaudrate = .baud19200
        case "38400":
            gBaudrate = .baud38400
        case "57600":
            gBaudrate = .baud57600
        case "115200":
            gBaudrate = .baud115200
        default:
            gBaudrate = .baud9600
        }
        print("fmBaudrateSelection: gBaudrate = \(gBaudrate)")
    }
    @IBAction func fnLaneSelection(_ sender: Any) {
        let popButton: NSPopUpButton = sender as! NSPopUpButton
        let laneStr: String = (popButton.selectedItem?.title)!
        switch laneStr {
        case "2":
            gLane = 2
        case "4":
            gLane = 4
        default:
            gLane = 4
        }
        print("fnLaneSelection: gLane = \(gLane)")
    }
    @IBAction func fnShowPinName(_ sender: Any) {
        if ckName.state == NSOnState {
            ckA12.title = "GND"
            ckA11.title = "RX2+"
            ckA10.title = "RX2-"
            ckA09.title = "VBUS"
            ckA08.title = "SBU1"
            ckA07.title = "D-"
            ckA06.title = "D+"
            ckA05.title = "CC1"
            ckA04.title = "VBUS"
            ckA03.title = "TX1-"
            ckA02.title = "TX1+"
            ckA01.title = "GND"

            ckB01.title = "GND"
            ckB02.title = "TX2+"
            ckB03.title = "TX2-"
            ckB04.title = "VBUS"
            ckB05.title = "CC2"
            ckB06.title = "D+"
            ckB07.title = "D-"
            ckB08.title = "SBU2"
            ckB09.title = "VBUS"
            ckB10.title = "RX1-"
            ckB11.title = "RX1+"
            ckB12.title = "GND"
        }else{
            ckA12.title = "A12"
            ckA11.title = "A11"
            ckA10.title = "A10"
            ckA09.title = "A09"
            ckA08.title = "A08"
            ckA07.title = "A07"
            ckA06.title = "A06"
            ckA05.title = "A05"
            ckA04.title = "A04"
            ckA03.title = "A03"
            ckA02.title = "A02"
            ckA01.title = "A01"

            ckB01.title = "B01"
            ckB02.title = "B02"
            ckB03.title = "B03"
            ckB04.title = "B04"
            ckB05.title = "B05"
            ckB06.title = "B06"
            ckB07.title = "B07"
            ckB08.title = "B08"
            ckB09.title = "B09"
            ckB10.title = "B10"
            ckB11.title = "B11"
            ckB12.title = "B12"
        }
    }
    
    
    @IBAction func fConnect(_ sender: Any) {
        if serialPortConnector.title == "Connect" {
            let portName:String = "/dev/cu." + (serialPortSelection.selectedItem?.title)!
            print("portName = \(portName)")
            serialPort = SerialPort(path: portName)
            do{
                try serialPort.openPort()
                serialPort.setSettings(receiveRate: gBaudrate, transmitRate: gBaudrate, minimumBytesToRead: 1)
                print("Serial port opened successfully.")
                serialPortConnector.title = "Disconnect"
                switchWidgets(state: true)
                m_isGlobalStop = false
            } catch PortError.failedToOpen {
                print("Serial port failed to open. You might need root permissions.")
            } catch {
                print("Error: \(error)")
            }
        }else{
            serialPort.closePort()
            print("Port Closed")
            m_isGlobalStop = true
            serialPortConnector.title = "Connect"
            switchWidgets(state: false)
            switchTestTime(false)
            switchProgressTimer(false)
            switchPDTimer(state: false)
        }
    }
    func OnePortOneFaceTest(testtype:TestType) -> Bool{
        bEDID = false;
        m_nTestSingCount += 1
        m_nTestAutoCount += 1
        //clear the content of list msg
        DispatchQueue.main.sync {
            self.listResult.string = ""
            self.listRecv.string = ""
            switch testtype {
            case .TEST_TYPE_APORT_A, .TEST_TYPE_APORT_B:
                self.btnPDTest.isEnabled = false
                self.btnBATest.isEnabled = false
                self.btnBBTest.isEnabled = false
                self.btnAutoTest.isEnabled = false
            case .TEST_TYPE_BPORT_A, .TEST_TYPE_BPORT_B:
                self.btnPDTest.isEnabled = false
                self.btnAATest.isEnabled = false
                self.btnABTest.isEnabled = false
                self.btnAutoTest.isEnabled = false
            default:
                self.btnPDTest.isEnabled = false
                self.btnBATest.isEnabled = false
                self.btnBBTest.isEnabled = false
                self.btnAutoTest.isEnabled = false
            }
            bEDID = ckDP.state == NSOnState
        }
        
        m_nTestSingCount += 1
        m_nTestAutoCount += 1
        var testItem: Commands = .TYPEA_A_GO
        switch testtype {
        case .TEST_TYPE_APORT_A, .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_A :
            testItem = .TYPEA_A_GO
            m_bAFaceTest = true
        case .TEST_TYPE_APORT_B, .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_B:
            testItem = .TYPEA_B_GO
            m_bBFaceTest = true
        case .TEST_TYPE_BPORT_A, .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_A:
            testItem = .TYPEB_A_GO
            m_bAFaceTest = true
        case .TEST_TYPE_BPORT_B, .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_B:
            testItem = .TYPEB_B_GO
            m_bBFaceTest = true
        default:
            testItem = .TYPEA_A_GO
        }
        m_nTestSingCount += 1
        m_nTestAutoCount += 1
        
        _ = sendRecv(testItem.rawValue, false)
        
        if bEDID {
            mlogger?.error("PD checked")
            //_ = sendRecv(testItem.rawValue)
        }else{
            mlogger?.error("PD not checked checked")
        }
        
        //Get VBUS voltage to decide whether CC lane is OK
        testItem = .TYPEV_I_GO
        var byteArray = sendRecv(testItem.rawValue)
        if m_isGlobalStop || byteArray.count == 0 {
            return true
        }
//        while !m_isGlobalStop && byteArray.count == 0 {
//            byteArray = sendRecv(testItem.rawValue)
//        }
        
        //Translate HEX to decimal value
        var tempU16: [UInt8] = [byteArray[1], byteArray[0]]
        let vs = UnsafePointer(tempU16).withMemoryRebound(to: UInt16.self, capacity: 1) { $0.pointee }
        tempU16 = [byteArray[3], byteArray[2]]
        let vm = UnsafePointer(tempU16).withMemoryRebound(to: UInt16.self, capacity: 1) { $0.pointee }
        tempU16 = [byteArray[5], byteArray[4]]
        let im = UnsafePointer(tempU16).withMemoryRebound(to: UInt16.self, capacity: 1) { $0.pointee }
        mlogger?.error("vs: \(vs), vm: \(vm), im: \(im)")
        if vs == 0 {
            return false
        }
        var measvol:Double = 0
        var meascurr:Double = 0
        measvol = 10.09 * 1.2 * Double(vm) / Double(vs)
        //meascurr = (1.2 * Double(im) / Double(vs) - 2.46) / 0.185
        meascurr = (2.4 * Double(im) / Double(vs))
        if meascurr > 1.0 && meascurr < 2.0 {
            meascurr = meascurr + MINCOMPENSATION
        }
        if meascurr > 2.0 {
            meascurr = meascurr + MAXCOMPENSATION
        }
        DispatchQueue.main.sync {
            var strmsg: String
            let volStr = NSString.init(format: "%.2f", measvol)
            let currStr = NSString.init(format:"%.2f", meascurr)
            if ckDP.state  == NSOnState {
                strmsg = "Charging Results: VBUS:\(volStr)V, CURR:\(currStr)A\n"
            } else {
                strmsg = "Measure Results: VCC1:\(volStr)V, CURR:\(currStr)A\n"
            }
            EmitMsgToListMessage(1, strmsg)
        
            //update the value to debug ui
            if ckDP.state  == NSOnState {
                lvbus.stringValue = "VBUS:\(volStr)V"
            }else{
                lvcc1.stringValue = "VCC1:\(volStr)V"
            }
            lisens.stringValue = "ISENS:\(currStr) A"
        }
        
        m_nTestSingCount += 1
        m_nTestAutoCount += 1
        mlogger?.error("measvol = \(measvol)")
        if(setColor(measvol: measvol)){
            //Get USB2.0 information
            if(m_bIsUSB2Test){
                mlogger?.debug("Doing m_bIsUSB2Test")
                testItem = .TYPEF_T_GO
                var lprecvdata = sendRecv(testItem.rawValue)
                if m_isGlobalStop || lprecvdata.count == 0 {
                    return true
                }
                m_nTestSingCount += 1
                m_nTestAutoCount += 1
                
                var tempU16: [UInt8] = [lprecvdata[0]]
                var usbstatus = UnsafePointer(tempU16).withMemoryRebound(to: Int8.self, capacity: 1) { $0.pointee }
                mlogger?.error("usbstatus = \(usbstatus)")
                if(usbstatus != 3){
                    testItem = .TYPEF_T_GO
                    lprecvdata = sendRecv(testItem.rawValue)
                    if m_isGlobalStop || lprecvdata.count == 0 {
                        return true
                    }
                    tempU16 = [lprecvdata[0]]
                    usbstatus = UnsafePointer(tempU16).withMemoryRebound(to: Int8.self, capacity: 1) { $0.pointee }
                }
                DispatchQueue.main.sync {
                    if(3 == usbstatus){
                        m_bIsUSB2Connected = true
                        EmitMsgToListMessage(1,"USB2.0 Connected");
                        if((m_nTestType == .TEST_TYPE_APORT_A) ||
                            (m_nTestType == .TEST_TYPE_APORT_B) ||
                            (m_nTestType == .TEST_TYPE_BPORT_A) ||
                            (m_nTestType == .TEST_TYPE_BPORT_B)){
                            if((m_nTestType == .TEST_TYPE_APORT_A) || (m_nTestType == .TEST_TYPE_BPORT_A)){
                                m_nIsConnectionPassA[6] = 1;
                                ckA07.state = NSOnState
    //                            cka07->setStyleSheet("background-color:rgb(0,255,0)");
                                
                                m_nIsConnectionPassA[5] = 1;
                                ckA06.state = NSOnState
    //                            ui.cka06->setStyleSheet("background-color:rgb(0,255,0)");
                                EmitMsgToListMessage(1,"USB2.0 test of A face Pass!");
                            }else if((m_nTestType == .TEST_TYPE_APORT_B) || (m_nTestType == .TEST_TYPE_BPORT_B)){
                                m_nIsConnectionPassB[5] = 1;
                                ckA06.state = NSOnState
    //                            ui.ckb06->setStyleSheet("background-color:rgb(0,255,0)");
                                
                                m_nIsConnectionPassB[6] = 1;
                                ckA07.state = NSOnState
    //                            ui.ckb07->setStyleSheet("background-color:rgb(0,255,0)");
                                EmitMsgToListMessage(1,"USB2.0 test of B face Pass!");
                            }
                            if(m_bIsVbusDet){
                                m_bIsTestSingPass = true
                            }
                        }else if(m_nTestType == .TEST_TYPE_AUTO){
                            if((m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_A) ||
                                (m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_B) ||
                                (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_A) ||
                                (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_B)){
                                if((m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_A) ||
                                    (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_A)){
                                    m_nIsConnectionPassA[6] = 1;
                                    ckA07.state = NSOnState
    //                                ui.cka07->setStyleSheet("background-color:rgb(0, 255, 0)");
                                    m_nIsConnectionPassA[5] = 1;
                                    ckA06.state = NSOnState
    //                                ui.cka06->setStyleSheet("background-color:rgb(0, 255, 0)");
                                    
                                    EmitMsgToListMessage(1,"AUTO test USB2.0 with A face Pass!");
                                }else if((m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_B) ||
                                    (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_B)){
                                    m_nIsConnectionPassB[5] = 1;
                                    ckB06.state = NSOnState
    //                                ui.ckb06->setStyleSheet("background-color:rgb(0, 255, 0)");
                                    m_nIsConnectionPassB[6] = 1;
                                    ckB07.state = NSOnState
    //                                ui.ckb07->setStyleSheet("background-color:rgb(0, 255, 0)");
                                    
                                    EmitMsgToListMessage(1,"AUTO test USB2.0 with B face Pass!");
                                }
                                m_bIsTestAutoUSB2Pass = true
                            }
                        }
                        lusb20.stringValue = "USB2.0:Connected"
                        m_nTestSingCount += 1
                        m_nTestAutoCount += 1
                    }else{
                        lusb20.stringValue = "USB2.0:Disconnected"
                        EmitMsgToListMessage(1, "USB2.0:Disconnected");
                        m_nTestSingCount += 1
                        m_nTestAutoCount += 1
                    }
                }//main.sync
            }else{
                mlogger?.error("skip m_bIsUSB2Test")
            }
            
            if(m_bIsDPTest){
                //Get DPCD information of PS176
                //0x00~0x0F: DPCD0000h~DPCD0000Fh(sink capability).
                //0x20~0x2E: DPCD100h~DPCD10Eh(link field)
                //0x30~0x37: DPCD200h~DPCD207h(link status)
                //0x38~0x3F: DPCD210h~DPCD217h(link err)
                mlogger?.debug("continue do m_bIsDPTest")
                let testItem:Commands = .TYPEP_S_GO
                var lprecvdata = sendRecv(testItem.rawValue)
                if m_isGlobalStop || lprecvdata.count == 0 {
                    return true
                }
                m_nTestSingCount += 1
                m_nTestAutoCount += 1
                
                //Parse DPCD data from what we get from PS176 Register.
                let numcount:Int = lprecvdata.count
                
                for numindex in 0...47{
                    if numindex < 16 {
                        m_bDPSinkCapability[numindex] = lprecvdata[numindex]
                    }else if((numindex>15) && (numindex<32)){
                        m_bDPLinkField[numindex-16] = lprecvdata[numindex]
                    }else if((numindex>31) && (numindex<40)){
                        m_bDPLinkStatus[numindex-32] = lprecvdata[numindex]
                    }else{
                        m_bDPLinkErr[numindex-40] = lprecvdata[numindex]
                    }
                }
                m_nTestSingCount += 1
                m_nTestAutoCount += 1
                
                //Check DP link capability
                _ = CheckDPResult(0)
                usleep(useconds_t(USLEEP_INTERVEL))
                m_nTestSingCount += 1
                m_nTestAutoCount += 1
                
                //Check DP Link Field
                _ = CheckDPResult(1);
                usleep(useconds_t(USLEEP_INTERVEL))
                m_nTestSingCount += 1
                m_nTestAutoCount += 1
                
                //Check DP Link Status
                _ = CheckDPResult(2);
                usleep(useconds_t(USLEEP_INTERVEL))
                m_nTestSingCount += 1
                m_nTestAutoCount += 1
                
                //Check DP Link Error
                ShowDPResult();
                m_nTestSingCount += 1
                m_nTestAutoCount += 1
                
                let bResult:Bool = CheckDPResult(3);
                DispatchQueue.main.sync {
                    if((m_nTestType == .TEST_TYPE_APORT_A) ||
                        (m_nTestType == .TEST_TYPE_APORT_B) ||
                        (m_nTestType == .TEST_TYPE_BPORT_A) ||
                        (m_nTestType == .TEST_TYPE_BPORT_B)){
                        if(bResult){
                            var lanecount:Int = Int(cbline.title)!
                            if(lanecount == 2){
                                lanecount = Int(cbline.title)!
                            }else{
                                lanecount = Int(m_bDPSinkCapability[2] & 0x1F)
                            }
                            //First of all, SBU1 and SBU2 are OK.
                            m_nIsConnectionPassA[7] = 1;
                            ckA08.state = NSOnState
    //                        ui.cka08->setStyleSheet("background-color:rgb(0, 255, 0)");
                            
                            m_nIsConnectionPassB[7] = 1;
                            ckB08.state = NSOnState
    //                        ui.ckb08->setStyleSheet("background-color:rgb(0, 255, 0)");
                            m_nTestSingCount += 1
                            m_nTestAutoCount += 1
                            
                            if(lanecount==4){
                                //TX1+,TX1-, RX2+, RX2-, TX2+,TX2-, RX1+, RX1-
                                if(m_nIsConnectionPassA[10] == 1){
                                    ckA11.state = NSOnState
    //                                ui.cka11->setStyleSheet("background-color:rgb(0, 255, 0)");
                                }
                                
                                if(m_nIsConnectionPassA[9] == 1){
                                    ckA10.state = NSOnState
    //                                ui.cka10->setStyleSheet("background-color:rgb(0, 255, 0)");
                                }
                                
                                if(m_nIsConnectionPassA[1] == 1){
                                    ckA02.state = NSOnState
    //                                ui.cka02->setStyleSheet("background-color:rgb(0, 255, 0)");
                                }
                                
                                if(m_nIsConnectionPassA[2] == 1){
                                    ckA03.state = NSOnState
    //                                ui.cka03->setStyleSheet("background-color:rgb(0, 255, 0)");
                                }
                                
                                if(m_nIsConnectionPassB[1] == 1){
                                    ckB02.state = NSOnState
    //                                ui.ckb02->setStyleSheet("background-color:rgb(0, 255, 0)");
                                }
                                
                                if(m_nIsConnectionPassB[2] == 1){
                                    ckB03.state = NSOnState
    //                                ui.ckb03->setStyleSheet("background-color:rgb(0, 255, 0)");
                                }
                                
                                if(m_nIsConnectionPassB[10] == 1){
                                    ckB11.state = NSOnState
    //                                ui.ckb11->setStyleSheet("background-color:rgb(0, 255, 0)");
                                }
                                
                                if(m_nIsConnectionPassB[9] == 1){
                                    ckB10.state = NSOnState
    //                                ui.ckb10->setStyleSheet("background-color:rgb(0, 255, 0)");
                                }
                            }
                            if(lanecount == 2){
                                if((m_nTestType == .TEST_TYPE_APORT_A) || (m_nTestType == .TEST_TYPE_BPORT_A)){
                                    if(m_nIsConnectionPassB[1] == 1){
                                        ckB02.state = NSOnState
    //                                    ui.ckb02->setStyleSheet("background-color:rgb(0,255,0)");
                                    }
                                    
                                    if(m_nIsConnectionPassB[2] == 1){
                                        ckB03.state = NSOnState
    //                                    ui.ckb03->setStyleSheet("background-color:rgb(0,255,0)");
                                    }
                                    
                                    if(m_nIsConnectionPassA[9] == 1){
                                        ckA10.state = NSOnState
    //                                    ui.cka10->setStyleSheet("background-color:rgb(0,255,0)");
                                    }
                                    
                                    if(m_nIsConnectionPassA[10] == 1){
                                        ckA11.state = NSOnState
    //                                    ui.cka11->setStyleSheet("background-color:rgb(0,255,0)");
                                    }
                                }else if((m_nTestType == .TEST_TYPE_APORT_B) || (m_nTestType == .TEST_TYPE_BPORT_B)){
                                    if(m_nIsConnectionPassA[1] == 1){
                                        ckA02.state = NSOnState
    //                                    ui.cka02->setStyleSheet("background-color:rgb(0,255,0)");
                                    }
                                    
                                    if(m_nIsConnectionPassA[2] == 1){
                                        ckB03.state = NSOnState
    //                                    ui.cka03->setStyleSheet("background-color:rgb(0,255,0)");
                                    }
                                    
                                    if(m_nIsConnectionPassB[9] == 1){
                                        ckB10.state = NSOnState
    //                                    ui.ckb10->setStyleSheet("background-color:rgb(0,255,0)");
                                    }
                                    
                                    if(m_nIsConnectionPassB[10] == 1){
                                        ckB11.state = NSOnState
    //                                    ui.ckb11->setStyleSheet("background-color:rgb(0,255,0)");
                                    }
                                }
                            }
                            
                            m_bIsDPCDGotten = true;
                            m_nTestSingCount += 1
                            m_nTestAutoCount += 1
                            
                            if(!m_bIsDispConnected)
                            {
                                if(m_bIsVbusDet)
                                {
                                    m_bIsTestSingPass = true;
                                }
                            }
                        }
                    }else if(m_nTestType == .TEST_TYPE_AUTO){
                        if((m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_A) ||
                            (m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_B) ||
                            (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_A) ||
                            (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_B)){
                            if(bResult){
                                var lanecount:Int = Int(cbline.title)!
                                if(lanecount==2){
                                    lanecount = Int(cbline.title)!
                                }else{
                                    lanecount = Int(m_bDPSinkCapability[2] & 0x1F)
                                }
                                //First of all, SBU1 and SBU2 are OK.
                                m_nIsConnectionPassA[7] = 1;
                                ckA08.state = NSOnState
    //                            ui.cka08->setStyleSheet("background-color:rgb(0, 255, 0)");
                                
                                m_nIsConnectionPassB[7] = 1;
                                ckB08.state = NSOnState
    //                            ui.ckb08->setStyleSheet("background-color:rgb(0, 255, 0)");
                                m_nTestSingCount += 1
                                m_nTestAutoCount += 1
                                
                                if(lanecount == 4){
                                    //TX1+,TX1-, RX2+, RX2-, TX2+,TX2-, RX1+, RX1-
                                    if(m_nIsConnectionPassA[10] == 1){
                                        ckA11.state = NSOnState
    //                                    ui.cka11->setStyleSheet("background-color:rgb(0, 255, 0)");
                                    }
                                    
                                    if(m_nIsConnectionPassA[9] == 1){
                                        ckA10.state = NSOnState
    //                                    ui.cka10->setStyleSheet("background-color:rgb(0, 255, 0)");
                                    }
                                    
                                    if(m_nIsConnectionPassA[1] == 1){
                                        ckA02.state = NSOnState
    //                                    ui.cka02->setStyleSheet("background-color:rgb(0, 255, 0)");
                                    }
                                    
                                    if(m_nIsConnectionPassA[2] == 1){
                                        ckA03.state = NSOnState
    //                                    ui.cka03->setStyleSheet("background-color:rgb(0, 255, 0)");
                                    }
                                    
                                    if(m_nIsConnectionPassB[1] == 1){
                                        ckB02.state = NSOnState
    //                                    ui.ckb02->setStyleSheet("background-color:rgb(0, 255, 0)");
                                    }
                                    
                                    if(m_nIsConnectionPassB[2] == 1){
                                        ckB03.state = NSOnState
    //                                    ui.ckb03->setStyleSheet("background-color:rgb(0, 255, 0)");
                                    }
                                    
                                    if(m_nIsConnectionPassB[10] == 1){
                                        ckB11.state = NSOnState
    //                                    ui.ckb11->setStyleSheet("background-color:rgb(0, 255, 0)");
                                    }
                                    
                                    if(m_nIsConnectionPassB[9] == 1){
                                        ckB10.state = NSOnState
    //                                    ui.ckb10->setStyleSheet("background-color:rgb(0, 255, 0)");
                                    }
                                }
                                if(lanecount == 2){
                                    if((m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_A) ||
                                        (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_A)){
                                        if(m_nIsConnectionPassB[1] == 1){
                                            ckB02.state = NSOnState
    //                                        ui.ckb02->setStyleSheet("background-color:rgb(0,255,0)");
                                        }
                                        
                                        if(m_nIsConnectionPassB[2] == 1){
                                            ckB03.state = NSOnState
    //                                        ui.ckb03->setStyleSheet("background-color:rgb(0,255,0)");
                                        }
                                        
                                        if(m_nIsConnectionPassA[9] == 1){
                                            ckA10.state = NSOnState
    //                                        ui.cka10->setStyleSheet("background-color:rgb(0,255,0)");
                                        }
                                        
                                        if(m_nIsConnectionPassA[10] == 1){
                                            ckA11.state = NSOnState
    //                                        ui.cka11->setStyleSheet("background-color:rgb(0,255,0)");
                                        }
                                    }
                                    else if((m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_B) ||
                                        (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_B)){
                                        if(m_nIsConnectionPassA[1] == 1){
                                            ckA02.state = NSOnState
    //                                        cka02->setStyleSheet("background-color:rgb(0,255,0)");
                                        }
                                        
                                        if(m_nIsConnectionPassA[2] == 1){
                                            ckA03.state = NSOnState
    //                                        ui.cka03->setStyleSheet("background-color:rgb(0,255,0)");
                                        }
                                        
                                        if(m_nIsConnectionPassB[9] == 1){
                                            ckB10.state = NSOnState
    //                                        ui.ckb10->setStyleSheet("background-color:rgb(0,255,0)");
                                        }
                                        
                                        if(m_nIsConnectionPassB[10] == 1){
                                            ckB11.state = NSOnState
    //                                        ui.ckb11->setStyleSheet("background-color:rgb(0,255,0)");
                                        }
                                    }
                                }
                            }
                            
                            m_bIsDPCDGotten = true;
                            m_nTestSingCount += 1
                            m_nTestAutoCount += 1
                            
                            if(!m_bIsDispConnected){
                                m_bIsTestAutoDPPass = true;
                            }
                        }
                    }
                    if(numcount<2) {
                        EmitMsgToListMessage(1,"Get DPCD information failed");
                        //                    pLog->OutLog("Get DPCD information failed");
                        m_bIsDPCDGotten = false;
                        m_nTestSingCount += 1
                        m_nTestAutoCount += 1
                    }
                } // main.sync
                //Following Get EDID information
                if true {
                    for ecount in 0...5 {
                        //First of all, get head content of EDID data
                        let testItem: Commands = .TYPED_D_GO
                        var lprecvdata = sendRecv(testItem.rawValue)
                        if m_isGlobalStop || lprecvdata.count == 0 {
                            return false
                        }
                        m_nTestSingCount += 1
                        m_nTestAutoCount += 1
                        
                        switch(ecount){
                        case 0:
                            for i in 0...17 {
                                m_bEDIDHead[i] = lprecvdata[i]
                            }
                            _ = CheckEDIDResult(0);
                            m_nTestSingCount += 1
                            m_nTestAutoCount += 1
                            break;
                            
                        case 1:
                            for i in 0...6 {
                                m_bEDIDBase[i] = lprecvdata[i];
                            }
                            _ = CheckEDIDResult(1);
                            m_nTestSingCount += 1
                            m_nTestAutoCount += 1
                            break;
                            
                        case 2:
                            for i in 0...17 {
                                m_bEDIDTiming0[i] = lprecvdata[i]
                            }
                            _ = CheckEDIDResult(2);
                            m_nTestSingCount += 1
                            m_nTestAutoCount += 1
                            break;
                            
                        case 3:
                            for i in 0...17 {
                                m_bEDIDTiming1[i] = lprecvdata[i]
                            }
                            _ = CheckEDIDResult(3);
                            m_nTestSingCount += 1
                            m_nTestAutoCount += 1
                            break;
                            
                        case 4:
                            for i in 0...17 {
                                m_bEDIDTiming2[i] = lprecvdata[i]
                            }
                            _ = CheckEDIDResult(4);
                            m_nTestSingCount += 1
                            m_nTestAutoCount += 1
                            break;
                            
                        case 5:
                            for i in 0...17 {
                                m_bEDIDTiming3[i] = lprecvdata[i]
                            }
                            ShowEDIDResult();
                            _ = CheckEDIDResult(5);
                            m_nTestSingCount += 1
                            m_nTestAutoCount += 1
                            break;
                        default:
                            for i in 0...17 {
                                m_bEDIDHead[i] = lprecvdata[i]
                            }
                            _ = CheckEDIDResult(0);
                            break;
                        }
                    }
                    DispatchQueue.main.sync {
                        if((m_nTestType == .TEST_TYPE_APORT_A) ||
                            (m_nTestType == .TEST_TYPE_APORT_B) ||
                            (m_nTestType == .TEST_TYPE_BPORT_A) ||
                            (m_nTestType == .TEST_TYPE_BPORT_B)){
                            if(m_bIsVbusDet && m_bIsDPCDGotten){
                                m_bIsEDIDGotten = true;
                                m_bIsTestSingPass = true;
                            }
                        }else if(m_nTestType == .TEST_TYPE_AUTO){
                            if(m_bIsVbusDet && m_bIsDPCDGotten){
                                m_bIsEDIDGotten = true;
                                m_bIsTestAutoDPPass = true;
                                if((m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_A) ||
                                    (m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_B) ||
                                    (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_A) ||
                                    (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_B)){
                                    EmitMsgToListMessage(1,"AUTO test DP with A face Pass!");
                                }else{
                                    EmitMsgToListMessage(1,"AUTO test DP with B face Pass!");
                                }
                            }
                        }
                    }
                    m_bIsGetEDID = true;
                    m_nTestSingCount += 1
                    m_nTestAutoCount += 1
                }
            }else{
                mlogger?.error("skip m_bIsDPTest")
            }
        }else{
            mlogger?.error("setColor false")
        }
        //Judge pass or fail of A face or B face
        //if(m_bIsVbusDet && m_bIsUSB2Connected && m_bIsDispConnected && m_bIsDPCDGotten && m_bIsEDIDGotten)
        if(m_bIsVbusDet && m_bIsUSB2Connected && m_bIsDPCDGotten && m_bIsEDIDGotten){
            if((m_nTestType == .TEST_TYPE_APORT_A) || (m_nTestType == .TEST_TYPE_BPORT_A)){
                ShowFuncResultInfo(m_nTestType,"Pass");
                m_bAFacePass = true;
            }else if((m_nTestType == .TEST_TYPE_APORT_B) || (m_nTestType == .TEST_TYPE_BPORT_B)){
                ShowFuncResultInfo(m_nTestType,"Pass");
                m_bBFacePass = true;
            }else if((m_nTestType == .TEST_TYPE_AUTO) &&
                ((m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_A) ||
                    (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_A))){
                ShowFuncResultInfo(m_nTestAutoType,"Pass");
                m_bAFacePass = true;
            }else if((m_nTestType == .TEST_TYPE_AUTO) &&
                (m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_B) ||
                (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_B)){
                ShowFuncResultInfo(m_nTestAutoType,"Pass");
                m_bBFacePass = true;
            }
            m_nTestSingCount += 1
            m_nTestAutoCount += 1
        }else{
            if((m_nTestType == .TEST_TYPE_APORT_A) || (m_nTestType == .TEST_TYPE_BPORT_A)){
                ShowFuncResultInfo(m_nTestType,"Fail");
                m_bAFacePass = false;
            }else if((m_nTestType == .TEST_TYPE_APORT_B) || (m_nTestType == .TEST_TYPE_BPORT_B)){
                ShowFuncResultInfo(m_nTestType,"Fail");
                m_bBFacePass = false;
            }else if((m_nTestType == .TEST_TYPE_AUTO) &&
                ((m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_A) ||
                    (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_A))){
                ShowFuncResultInfo(m_nTestAutoType,"Fail");
                m_bAFacePass = false;
            }else if((m_nTestType == .TEST_TYPE_AUTO) &&
                (m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_B) ||
                (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_B)){
                ShowFuncResultInfo(m_nTestAutoType,"Fail");
                m_bBFacePass = false;
            }
            m_nTestSingCount += 1
            m_nTestAutoCount += 1
        }
        
        //change the background color of one port test complete.
        DispatchQueue.main.sync {
            if(m_bAFaceTest && m_bBFaceTest){
                mlogger?.debug("turn on all")
                //Enable all button can work
                switchWidgets(state: true)
            }else{
                if(m_nTestType == .TEST_TYPE_APORT_A){
                    mlogger?.debug("turn on AA")
                    btnAATest.isEnabled = true
                }else if(m_nTestType == .TEST_TYPE_APORT_B){
                    mlogger?.debug("turn on AB")
                    btnABTest.isEnabled = true
                }else if(m_nTestType == .TEST_TYPE_BPORT_A){
                    mlogger?.debug("turn on BA")
                    btnBATest.isEnabled = true
                }else if(m_nTestType == .TEST_TYPE_BPORT_B){
                    mlogger?.debug("turn on BB")
                    btnBBTest.isEnabled = true
                }else{
                    mlogger?.debug("unknow test type")
                }
            }
        }
        mlogger?.error("Test Complete!")
        return true
    }
    
    func CheckDPResult(_ type:Int) -> Bool{
        if(type == 0){
            var buffer:String = ""
            DispatchQueue.main.sync {
            EmitMsgToListMessage(1,"DP SinkCapability(0000h-000Fh):");
            }
            let arg1 = m_bDPSinkCapability[0]>>4
            let arg2 = m_bDPSinkCapability[0]&0xF
            let arg3 = (m_bDPSinkCapability[1] == 0x06) ? (1.62) : (
                (m_bDPSinkCapability[1] == 0x0A) ? (2.7) : (
                    (m_bDPSinkCapability[1] == 0x14) ? (5.4) : (0)))
            let arg4 = m_bDPSinkCapability[2]&0x1F
            let arg5 = ((m_bDPSinkCapability[3]&0x01) == 0) ? ("No down spread") : ("Up to 0.5% down spread")
            buffer = "DPCD_REV:\(arg1).\(arg2); MAX_LINK_RATE:\(arg3)Gbps; MAX_LANE_COUNT:\(arg4); MAX_DOWNSPREAD:\(arg5)"
            DispatchQueue.main.sync {
            EmitMsgToListMessage(1,buffer);
            }
            let arg11 = (m_bDPSinkCapability[4]&0x01)+1
            let arg12 = ((m_bDPSinkCapability[4]&0x20) > 0) ? (5) : (
                ((m_bDPSinkCapability[4]&0x40) > 0) ? (12) : (
                    ((m_bDPSinkCapability[4]&0x80) > 0) ? (18) : (3.3)))
            let arg13 = ((m_bDPSinkCapability[5]&0x01) == 0) ? ("No port") : (
                ((m_bDPSinkCapability[5]&0x06) == 0x00) ? ("DisplayPort") : (
                    ((m_bDPSinkCapability[5]&0x06) == 0x02) ? ("Analog VGA or analog video over DVI-I") : (
                        ((m_bDPSinkCapability[5]&0x06) == 0x04) ? ("DVI, HDMI or DP++") : ("Others"))))
            let arg14 = (m_bDPSinkCapability[6] == 0) ? ("Not support") : ("ANSI 8B/10B")
            let arg15 = m_bDPSinkCapability[7]&0x0F
            buffer = "NORP & DP_PWR_VOLTAGE_CAP:\(arg11)port,\(arg12)V_DP_PWR_CAP; DOWNSTREAMPORT:\(arg13); MAIN_LINK_CHANNEL_CODING:\(arg14); DOWN_STREAM_PORT_COUNT:\(arg15)"
            DispatchQueue.main.sync {
            EmitMsgToListMessage(1,buffer);
            }
            let arg21 = ((m_bDPSinkCapability[8]&0x02) == 0) ? ("No Local EDID") : ("Local EDID")
            let arg22 = (m_bDPSinkCapability[9]+1)*32
            let arg23 = ((m_bDPSinkCapability[10]&0x02) == 0) ? ("No Local EDID") : ("Local EDID")
            let arg24 = (m_bDPSinkCapability[11]+1)*32
            buffer = "RECEIVE_PORT0_CAP_0:\(arg21); RECEIVE_PORT0_CAP_1:\(arg22)bytes per lane; RECEIVE_PORT1_CAP_0:\(arg23); RECEIVE_PORT1_CAP_1:\(arg24)bytes per lane"
            DispatchQueue.main.sync {
            EmitMsgToListMessage(1,buffer);
            }
            let arg31 = ((m_bDPSinkCapability[12]&0x20) > 0) ? ("1M") : (
                ((m_bDPSinkCapability[12]&0x10) > 0) ? ("400K") : (
                    ((m_bDPSinkCapability[12]&0x08) > 0) ? ("100K") : (
                        ((m_bDPSinkCapability[12]&0x04) > 0) ? ("10K") : (
                            ((m_bDPSinkCapability[12]&0x02) > 0) ? ("5K") : (
                                ((m_bDPSinkCapability[12]&0x01) > 0) ? ("1K") : ("N/A"))))))
            let arg32 = ((m_bDPSinkCapability[13]&0x01) == 0) ? ("not eDP") : ("ALTERNATE_SCRAMBLER_RESET_CAPABLE")
            let arg33 = ((m_bDPSinkCapability[13]&0x02) == 0) ? ("not eDP") : ("FRAMING_CHANGE_CAPABLE")
            let arg34 = (m_bDPSinkCapability[14] == 0) ? ("100u") : (
                (m_bDPSinkCapability[14] == 1) ? ("4m") : (
                    (m_bDPSinkCapability[14] == 2) ? ("8m") : (
                        (m_bDPSinkCapability[14] == 3) ? ("12m") : (
                            (m_bDPSinkCapability[14] == 4) ? ("16m") : ("N/A")))))
            let arg35 = ((m_bDPSinkCapability[15]&0x01) == 0) ? ("not supports VGA force load") : ("supports VGA force load")
            let arg36 = ((m_bDPSinkCapability[13]&0x02) == 0) ? ("not supports alternate I2C patterns") : ("supports alternate I2C patterns")
            buffer = "I2C speed:\(arg31)bps; eDP_CONFIGURATION_CAP:\(arg32),\(arg33); TRAINING_AUX_RD_INTERVAL:\(arg34)s; ADAPTER_CAP:\(arg35),\(arg36)"
            DispatchQueue.main.sync {
            EmitMsgToListMessage(1,buffer);
            }
        }else if(type == 1){
            var buffer:String = ""
            DispatchQueue.main.sync {
            EmitMsgToListMessage(1,"DP LinkField(0100h-010Eh):");
            }
            let arg1 = (m_bDPLinkField[0] == 0x06) ? (1.62) : (
                (m_bDPLinkField[0] == 0x0A) ? (2.7) : (
                    (m_bDPLinkField[0] == 0x14) ? (5.4) : (0)))
            let arg2 = m_bDPLinkField[1]&0x1F
            let arg3 = ((m_bDPLinkField[2]&0x03) == 1) ? ("Pattern 1") : (
                ((m_bDPLinkField[2]&0x03) == 2) ? ("Pattern 2") : (
                    ((m_bDPLinkField[2]&0x03) == 3) ? ("Pattern 3") : ("not in progress or disabled")))
            buffer = "LINK_BW_SET:\(arg1)Gbps; LANE_COUNT_SET:\(arg2); TRAINING_PATTERN_SET:\(arg3)"
            DispatchQueue.main.sync {
            EmitMsgToListMessage(1,buffer);
            }
            let arg11 = ((m_bDPLinkField[3]&0x04) > 0) ? ("Max") : (
                ((m_bDPLinkField[3]&0x03) == 0) ? ("0") : (
                    ((m_bDPLinkField[3]&0x03) == 1) ? ("1") : (
                        ((m_bDPLinkField[3]&0x03) == 2) ? ("2") : ("3"))))
            let arg12 = ((m_bDPLinkField[3]&0x20) > 0) ? ("Max") : (
                ((m_bDPLinkField[3]&0x18) == 0) ? ("0") : (
                    ((m_bDPLinkField[3]&0x18) == 0x08) ? ("1") : (
                        ((m_bDPLinkField[3]&0x18) == 0x10) ? ("2") : ("3"))))
            let arg13 = ((m_bDPLinkField[4]&0x04) > 0) ? ("Max") : (
                ((m_bDPLinkField[4]&0x03) == 0) ? ("0") : (
                    ((m_bDPLinkField[4]&0x03) == 1) ? ("1") : (
                        ((m_bDPLinkField[4]&0x03) == 2) ? ("2") : ("3"))))
            let arg14 = ((m_bDPLinkField[4]&0x20) > 0) ? ("Max") : (
                ((m_bDPLinkField[4]&0x18) == 0) ? ("0") : (
                    ((m_bDPLinkField[4]&0x18) == 0x08) ? ("1") : (
                        ((m_bDPLinkField[4]&0x18) == 0x10) ? ("2") : ("3"))))
            buffer = "TRAINING_LANE0_SET:Vol swing lev \(arg11),Pre-emp lev \(arg12); TRAINING_LANE1_SET:Vol swing lev \(arg13),Pre-emp lev \(arg14)"
            DispatchQueue.main.sync {
            EmitMsgToListMessage(1,buffer);
            }
            let arg21 = ((m_bDPLinkField[5]&0x04) > 0) ? ("Max") : (
                ((m_bDPLinkField[5]&0x03) == 0) ? ("0") : (
                    ((m_bDPLinkField[5]&0x03) == 1) ? ("1") : (
                        ((m_bDPLinkField[5]&0x03) == 2) ? ("2") : ("3"))))
            let arg22 = ((m_bDPLinkField[5]&0x20) > 0) ? ("Max") : (
                ((m_bDPLinkField[5]&0x18) == 0) ? ("0") : (
                    ((m_bDPLinkField[5]&0x18) == 0x08) ? ("1") : (
                        ((m_bDPLinkField[5]&0x18) == 0x10) ? ("2") : ("3"))))
            let arg23 = ((m_bDPLinkField[6]&0x04) > 0) ? ("Max") : (
                ((m_bDPLinkField[6]&0x03) == 0) ? ("0") : (
                    ((m_bDPLinkField[6]&0x03) == 1) ? ("1") : (
                        ((m_bDPLinkField[6]&0x03) == 2) ? ("2") : ("3"))))
            let arg24 = ((m_bDPLinkField[6]&0x20) > 0) ? ("Max") : (
                ((m_bDPLinkField[6]&0x18) == 0) ? ("0") : (
                    ((m_bDPLinkField[6]&0x18) == 0x08) ? ("1") : (
                        ((m_bDPLinkField[6]&0x18) == 0x10) ? ("2") : ("3"))))
            buffer = "TRAINING_LANE2_SET:Vol swing lev \(arg21),Pre-emp lev \(arg22); TRAINING_LANE3_SET:Vol swing lev \(arg23),Pre-emp lev \(arg24)"
            DispatchQueue.main.sync {
            EmitMsgToListMessage(1,buffer);
            }
            let arg31 = ((m_bDPLinkField[7]&0x10) > 0) ? ("Main link signal is downspread") : ("Main link signal is not downspread")
            let arg32 = ((m_bDPLinkField[8]&0x01) == 0) ? ("None SET") : ("SET_ANSI 8B10B")
            let arg33 = ((m_bDPLinkField[9]&0x20) > 0) ? ("1M") : (
                ((m_bDPLinkField[9]&0x10) > 0) ? ("400K") : (
                    ((m_bDPLinkField[9]&0x08) > 0) ? ("100K") : (
                        ((m_bDPLinkField[9]&0x04) > 0) ? ("10K") : (
                            ((m_bDPLinkField[9]&0x02) > 0) ? ("5K") : (
                                ((m_bDPLinkField[9]&0x01) > 0) ? ("1K") : ("N/A"))))))
            let arg34 = ((m_bDPLinkField[10]&0x01) == 0) ? ("not eDP") : ("ALTERNATE_SCRAMBER_RESET_ENABLE")
            let arg35 = ((m_bDPLinkField[10]&0x02) == 0) ? ("not eDP") : ("FRAMING_CHANGE_ENABLE")
            buffer = "DOWNSPREAD_CTRL:\(arg31); MAIN_LINK_CHANNEL_CODING_SET:\(arg32); I2C speed:\(arg33)bps; eDP_CONFIGURATION_SET:\(arg34),\(arg35)"
            DispatchQueue.main.sync {
            EmitMsgToListMessage(1,buffer);
            }
            let arg41 = ((m_bDPLinkField[11]&0x07) == 0) ? ("Link quality test pattern not transmitted") : (
                ((m_bDPLinkField[11]&0x07) == 1) ? ("D10.2 test pattern transmitted") : (
                    ((m_bDPLinkField[11]&0x07) == 2) ? ("Symbol Error Rate Measurement Pattern transmitted") : (
                        ((m_bDPLinkField[11]&0x07) == 3) ? ("PRBS7 transmitted") : (
                            ((m_bDPLinkField[11]&0x07) == 4) ? ("80 bit custom pattern transmitted") : (
                                ((m_bDPLinkField[11]&0x07) == 5) ? ("HBR2 Compliance EYE pattern transmitted") : ("N/A"))))))
            let arg42 = ((m_bDPLinkField[12]&0x07) == 0) ? ("Link quality test pattern not transmitted") : (
                ((m_bDPLinkField[12]&0x07) == 1) ? ("D10.2 test pattern transmitted") : (
                    ((m_bDPLinkField[12]&0x07) == 2) ? ("Symbol Error Rate Measurement Pattern transmitted") : (
                        ((m_bDPLinkField[12]&0x07) == 3) ? ("PRBS7 transmitted") : (
                            ((m_bDPLinkField[12]&0x07) == 4) ? ("80 bit custom pattern transmitted") : (
                                ((m_bDPLinkField[12]&0x07) == 5) ? ("HBR2 Compliance EYE pattern transmitted") : ("N/A"))))))
            buffer = "LINK_QUAL_LANE0_SET:\(arg41); LINK_QUAL_LANE1_SET:\(arg42)"
            DispatchQueue.main.sync {
            EmitMsgToListMessage(1,buffer);
            }
            let arg43 = ((m_bDPLinkField[13]&0x07) == 0) ? ("Link quality test pattern not transmitted") : (
                ((m_bDPLinkField[13]&0x07) == 1) ? ("D10.2 test pattern transmitted") : (
                    ((m_bDPLinkField[13]&0x07) == 2) ? ("Symbol Error Rate Measurement Pattern transmitted") : (
                        ((m_bDPLinkField[13]&0x07) == 3) ? ("PRBS7 transmitted") : (
                            ((m_bDPLinkField[13]&0x07) == 4) ? ("80 bit custom pattern transmitted") : (
                                ((m_bDPLinkField[13]&0x07) == 5) ? ("HBR2 Compliance EYE pattern transmitted") : ("N/A"))))))
            let arg44 = ((m_bDPLinkField[14]&0x07) == 0) ? ("Link quality test pattern not transmitted") : (
                ((m_bDPLinkField[14]&0x07) == 1) ? ("D10.2 test pattern transmitted") : (
                    ((m_bDPLinkField[14]&0x07) == 2) ? ("Symbol Error Rate Measurement Pattern transmitted") : (
                        ((m_bDPLinkField[14]&0x07) == 3) ? ("PRBS7 transmitted") : (
                            ((m_bDPLinkField[14]&0x07) == 4) ? ("80 bit custom pattern transmitted") : (
                                ((m_bDPLinkField[14]&0x07) == 5) ? ("HBR2 Compliance EYE pattern transmitted") : ("N/A"))))))
            buffer = "LINK_QUAL_LANE2_SET:\(arg43); LINK_QUAL_LANE3_SET:\(arg44)"
            DispatchQueue.main.sync {
            EmitMsgToListMessage(1,buffer);
            }
        }else if(type == 2){
            var buffer:String = ""
            DispatchQueue.main.sync {
            EmitMsgToListMessage(1,"DP LinkStatus(0200h-0207h):");
            buffer = "SINK_COUNT:\(m_bDPLinkStatus[0]&0x1F); DEVICE_SERVICE_IRQ_VECTOR:0x\(String(format: "%2X", m_bDPLinkStatus[1]))"
            EmitMsgToListMessage(1,buffer);
            }
            let arg1 = ((m_bDPLinkStatus[2]&0x01) > 0) ? ("0_CR_DONE") : ("N/A")
            let arg2 = ((m_bDPLinkStatus[2]&0x02) > 0) ? ("0_CHANNEL_EQ_DONE") : ("N/A")
            let arg3 = ((m_bDPLinkStatus[2]&0x04) > 0) ? ("0_SYMBOL_LOCKED") : ("N/A")
            let arg4 = ((m_bDPLinkStatus[2]&0x10) > 0) ? ("1_CR_DONE") : ("N/A")
            let arg5 = ((m_bDPLinkStatus[2]&0x20) > 0) ? ("1_CHANNEL_EQ_DONE") : ("N/A")
            let arg6 = ((m_bDPLinkStatus[2]&0x40) > 0) ? ("1_SYMBOL_LOCKED") : ("N/A")
            buffer = "LANE0_1_STATUS:\(arg1)&\(arg2)&\(arg3),\(arg4)&\(arg5)&\(arg6)"
            DispatchQueue.main.sync {
            EmitMsgToListMessage(1,buffer);
            }
            let arg11 = ((m_bDPLinkStatus[3]&0x01) > 0) ? ("2_CR_DONE") : ("N/A")
            let arg12 = ((m_bDPLinkStatus[3]&0x02) > 0) ? ("2_CHANNEL_EQ_DONE") : ("N/A")
            let arg13 = ((m_bDPLinkStatus[3]&0x04) > 0) ? ("2_SYMBOL_LOCKED") : ("N/A")
            let arg14 = ((m_bDPLinkStatus[3]&0x10) > 0) ? ("3_CR_DONE") : ("N/A")
            let arg15 = ((m_bDPLinkStatus[3]&0x20) > 0) ? ("3_CHANNEL_EQ_DONE") : ("N/A")
            let arg16 = ((m_bDPLinkStatus[3]&0x40) > 0) ? ("3_SYMBOL_LOCKED") : ("N/A")
            buffer = "LANE2_3_STATUS:\(arg11)&\(arg12)&\(arg13),\(arg14)&\(arg15)&\(arg16)"
            DispatchQueue.main.sync {
            EmitMsgToListMessage(1,buffer);
            }
            let arg21 = ((m_bDPLinkStatus[4]&0x01) > 0) ? ("INTERLANE_ALIGN_DONE") : ("N/A")
            let arg22 = ((m_bDPLinkStatus[4]&0x40) > 0) ? ("DOWNSTREAM_PORT_STATUS_CHANGED") : ("N/A")
            let arg23 = ((m_bDPLinkStatus[4]&0x80) > 0) ? ("LINK_STATUS_UPDATED") : ("N/A")
            let arg24 = ((m_bDPLinkStatus[5]&0x01) > 0) ? ("PORT_0 SINK in sync") : ("PORT_0 SINK out of sync")
            let arg25 = ((m_bDPLinkStatus[5]&0x02) > 0) ? ("PORT_1 SINK in sync") : ("PORT_1 SINK out of sync")
            buffer = "LANE_ALIGN__STATUS_UPDATED:\(arg21)&\(arg22)&\(arg23); SINK_STATUS:\(arg24),\(arg25)"
            DispatchQueue.main.sync {
            EmitMsgToListMessage(1,buffer);
            }
            let arg31 = m_bDPLinkStatus[6]&0x03
            let arg32 = (m_bDPLinkStatus[6]&0x0C)>>2
            let arg33 = (m_bDPLinkStatus[6]&0x30)>>4
            let arg34 = (m_bDPLinkStatus[6]&0xC0)>>6
            let arg35 = m_bDPLinkStatus[7]&0x03
            let arg36 = (m_bDPLinkStatus[7]&0x0C)>>2
            let arg37 = (m_bDPLinkStatus[7]&0x30)>>4
            let arg38 = (m_bDPLinkStatus[7]&0xC0)>>6
            buffer = "ADJUST_REQUEST_LANE0_1:SW\(arg31)&EM\(arg32),SW\(arg33)&EM\(arg34); ADJUST_REQUEST_LANE2_3:SW\(arg35)&EM\(arg36),SW\(arg37)&EM\(arg38)"
            DispatchQueue.main.sync {
            EmitMsgToListMessage(1,buffer);
            }
        }else if(type == 3){
            var buffer:String = ""
            DispatchQueue.main.sync {
            EmitMsgToListMessage(1,"DP LinkErr(0210h-0217h):");
            }
            let arg1 = ((m_bDPLinkErr[1]&0x80) > 0) ? ("valid") : ("invalid")
            let arg2 = m_bDPLinkErr[0]+((m_bDPLinkErr[1]&0x7F)<<8)
            let arg3 = ((m_bDPLinkErr[3]&0x80) > 0) ? ("valid") : ("invalid")
            let arg4 = m_bDPLinkErr[2]+((m_bDPLinkErr[3]&0x7F)<<8)
            let arg5 = ((m_bDPLinkErr[5]&0x80) > 0) ? ("valid") : ("invalid")
            let arg6 = m_bDPLinkErr[4]+((m_bDPLinkErr[5]&0x7F)<<8)
            let arg7 = ((m_bDPLinkErr[7]&0x80) > 0) ? ("valid") : ("invalid")
            let arg8 = m_bDPLinkErr[6]+((m_bDPLinkErr[7]&0x7F)<<8)
            buffer = "SYMBOL_ERROR_COUNT_LANE0:\(arg1),\(arg2); SYMBOL_ERROR_COUNT_LANE1:\(arg3),\(arg4); SYMBOL_ERROR_COUNT_LANE2:\(arg5),\(arg6); SYMBOL_ERROR_COUNT_LANE3:\(arg7),\(arg8)"
            DispatchQueue.main.sync {
            EmitMsgToListMessage(1,buffer);
            }
            //In order to debug
            //int lanecount = m_bDPSinkCapability[2]&0x1F;
            var lanecount = gLane
            if lanecount == 2 {
                lanecount = gLane
            }else{
                lanecount = Int(m_bDPSinkCapability[2]&0x1F)
            }
            //This recognize the data lane is OK.
            m_nIsConnectionPassA[9] = 2;
            m_nIsConnectionPassA[10] = 2;
            m_nIsConnectionPassB[1] = 2;
            m_nIsConnectionPassB[2] = 2;
            m_nIsConnectionPassA[1] = 2;
            m_nIsConnectionPassA[2] = 2;
            m_nIsConnectionPassB[9] = 2;
            m_nIsConnectionPassB[10] = 2;
            
            if(4 == lanecount){//lanecount=4
                if(((m_bDPLinkErr[1]&0x80) > 0) && (Int(m_bDPLinkErr[0]+((m_bDPLinkErr[1]&0x7F)<<8)) < 1024)){
                    if(((m_nTestType == .TEST_TYPE_APORT_A)  ||
                        (m_nTestType == .TEST_TYPE_BPORT_A)) ||
                        ((m_nTestType == .TEST_TYPE_AUTO) &&
                            ((m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_A) ||
                                (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_A)  ||
                                (m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_A) ||
                                (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_A)))){
                        m_nIsConnectionPassB[1] = 1;
                        m_nIsConnectionPassB[2] = 1;
                    }else if(((m_nTestType == .TEST_TYPE_APORT_B) ||
                        (m_nTestType == .TEST_TYPE_BPORT_B)) ||
                            ((m_nTestType == .TEST_TYPE_AUTO) &&
                                ((m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_B) ||
                                    (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_B) ||
                                    (m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_B) ||
                                    (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_B)))){
                        m_nIsConnectionPassA[1] = 1;
                        m_nIsConnectionPassA[2] = 1;
                    }
                }
                if(((m_bDPLinkErr[3]&0x80) > 0) && (Int(m_bDPLinkErr[2]+((m_bDPLinkErr[3]&0x7F)<<8)) < 1024)){
                    if(((m_nTestType == .TEST_TYPE_APORT_A)  ||
                        (m_nTestType == .TEST_TYPE_BPORT_A)) ||
                        ((m_nTestAutoType == .TEST_TYPE_AUTO) &&
                            ((m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_A) ||
                                (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_A)  ||
                                (m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_A) ||
                                (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_A)))){
                        m_nIsConnectionPassA[9] = 1;
                        m_nIsConnectionPassA[10] = 1;
                    }else if(((m_nTestType == .TEST_TYPE_APORT_B) ||
                        (m_nTestType == .TEST_TYPE_BPORT_B)) ||
                            ((m_nTestType == .TEST_TYPE_AUTO) &&
                                ((m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_B) ||
                                    (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_B) ||
                                    (m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_B) ||
                                    (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_B)))){
                        m_nIsConnectionPassB[9] = 1;
                        m_nIsConnectionPassB[10] = 1;
                    }
                }
                if(((m_bDPLinkErr[5]&0x80) > 0) && (Int(m_bDPLinkErr[4]+((m_bDPLinkErr[5]&0x7F)<<8)) < 1024)){
                    if(((m_nTestType == .TEST_TYPE_APORT_A)  ||
                        (m_nTestType == .TEST_TYPE_BPORT_A)) ||
                        ((m_nTestAutoType == .TEST_TYPE_AUTO) &&
                            ((m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_A) ||
                                (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_A)  ||
                                (m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_A) ||
                                (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_A)))){
                        m_nIsConnectionPassA[1] = 1;
                        m_nIsConnectionPassA[2] = 1;
                    }else if(((m_nTestType == .TEST_TYPE_APORT_B) ||
                        (m_nTestType == .TEST_TYPE_BPORT_B)) ||
                            ((m_nTestType == .TEST_TYPE_AUTO) &&
                                ((m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_B) ||
                                    (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_B) ||
                                    (m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_B) ||
                                    (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_B)))){
                        m_nIsConnectionPassB[1] = 1;
                        m_nIsConnectionPassB[2] = 1;
                    }
                }
                if(((m_bDPLinkErr[7]&0x80) > 0) && (Int(m_bDPLinkErr[6]+((m_bDPLinkErr[7]&0x7F)<<8)) < 1024)){
                    if(((m_nTestType == .TEST_TYPE_APORT_A)  ||
                        (m_nTestType == .TEST_TYPE_BPORT_A)) ||
                        ((m_nTestAutoType == .TEST_TYPE_AUTO) &&
                            ((m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_A) ||
                                (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_A)  ||
                                (m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_A) ||
                                (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_A)))){
                        m_nIsConnectionPassB[9] = 1;
                        m_nIsConnectionPassB[10] = 1;
                    }else if(((m_nTestType == .TEST_TYPE_APORT_B) ||
                        (m_nTestType == .TEST_TYPE_BPORT_B)) ||
                            ((m_nTestType == .TEST_TYPE_AUTO) &&
                                ((m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_B) ||
                                    (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_B) ||
                                    (m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_B) ||
                                    (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_B)))){
                        m_nIsConnectionPassA[9] = 1;
                        m_nIsConnectionPassA[10] = 1;
                    }
                }
                
                if(((m_bDPLinkErr[1]&0x80) > 0) && ((m_bDPLinkErr[3]&0x80) > 0) && ((m_bDPLinkErr[5]&0x80) > 0) && ((m_bDPLinkErr[7]&0x80) > 0)){
                    if((Int(m_bDPLinkErr[0]+((m_bDPLinkErr[1]&0x7F)<<8)) < 1024) && (Int(m_bDPLinkErr[2]+((m_bDPLinkErr[3]&0x7F)<<8)) < 1024)
                        && (Int(m_bDPLinkErr[4]+((m_bDPLinkErr[5]&0x7F)<<8)) < 1024) && (Int(m_bDPLinkErr[6]+((m_bDPLinkErr[7]&0x7F)<<8)) < 1024)){
                        return true
                    }
                }
            }else if(2 == lanecount){
                if(((m_bDPLinkErr[1]&0x80) > 0) && (Int(m_bDPLinkErr[0]+((m_bDPLinkErr[1]&0x7F)<<8)) < 1024)){
                    if(((m_nTestType == .TEST_TYPE_APORT_A)  ||
                        (m_nTestType == .TEST_TYPE_BPORT_A)) ||
                        ((m_nTestType == .TEST_TYPE_AUTO) &&
                            ((m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_A) ||
                                (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_A)  ||
                                (m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_A) ||
                                (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_A)))){
                        m_nIsConnectionPassB[1] = 1;
                        m_nIsConnectionPassB[2] = 1;
                    }else if(((m_nTestType == .TEST_TYPE_APORT_B) ||
                        (m_nTestType == .TEST_TYPE_BPORT_B)) ||
                            ((m_nTestType == .TEST_TYPE_AUTO) &&
                                ((m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_B) ||
                                    (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_B) ||
                                    (m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_B) ||
                                    (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_B)))){
                        m_nIsConnectionPassA[1] = 1;
                        m_nIsConnectionPassA[2] = 1;
                    }
                }
                if(((m_bDPLinkErr[3]&0x80) > 0) && (Int(m_bDPLinkErr[2]+((m_bDPLinkErr[3]&0x7F)<<8)) < 1024)){
                    if(((m_nTestType == .TEST_TYPE_APORT_A)  ||
                        (m_nTestType == .TEST_TYPE_BPORT_A)) ||
                        ((m_nTestAutoType == .TEST_TYPE_AUTO) &&
                            ((m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_A) ||
                                (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_A)  ||
                                (m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_A) ||
                                (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_A)))){
                        m_nIsConnectionPassA[9] = 1;
                        m_nIsConnectionPassA[10] = 1;
                    }else if(((m_nTestType == .TEST_TYPE_APORT_B) ||
                        (m_nTestType == .TEST_TYPE_BPORT_B)) ||
                            ((m_nTestType == .TEST_TYPE_AUTO) &&
                                ((m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_B) ||
                                    (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_B) ||
                                    (m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_B) ||
                                    (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_B)))){
                        m_nIsConnectionPassB[9] = 1;
                        m_nIsConnectionPassB[10] = 1;
                    }
                }
                
                if(((m_bDPLinkErr[1]&0x80) > 0) && ((m_bDPLinkErr[3]&0x80) > 0)){
                    if((Int(m_bDPLinkErr[0]+((m_bDPLinkErr[1]&0x7F)<<8)) < 1024) && (Int(m_bDPLinkErr[2]+((m_bDPLinkErr[3]&0x7F)<<8)) < 1024)){
                        return true
                    }
                }
            }
        }
        return false
    }
    
    func CheckEDIDResult(_ type:Int) -> Bool {
        DispatchQueue.main.sync {
            if type == 0 {
                var buffer:String = ""
                EmitMsgToListMessage(1,"EDID Head(00h-11h):")
                
                if((m_bEDIDHead[0] != 0) || (m_bEDIDHead[1] != 0xFF) || (m_bEDIDHead[2] != 0xFF) || (m_bEDIDHead[3] != 0xFF)
                    || (m_bEDIDHead[4] != 0xFF) || (m_bEDIDHead[5] != 0xFF) || (m_bEDIDHead[6] != 0xFF) || (m_bEDIDHead[7] != 0)){
                    buffer = "Invalid data!"
                }else{
                    let arg1 = ((m_bEDIDHead[8]&0x7C)>>2)+65-1  //65 stands for 'A'
                    let arg2 = ((m_bEDIDHead[8]&0x3)<<3)+((m_bEDIDHead[9]&0xE0)>>5)+65-1
                    let arg3 = (m_bEDIDHead[9]&0x1F)+65-1
                    let arg4 = String(format:"%2X", m_bEDIDHead[11])
                    let arg5 = String(format:"%2X", m_bEDIDHead[10])
                    let arg6 = String(format:"%2X", m_bEDIDHead[15])
                    let arg7 = String(format:"%2X", m_bEDIDHead[14])
                    let arg8 = String(format:"%2X", m_bEDIDHead[13])
                    let arg9 = String(format:"%2X", m_bEDIDHead[12])
                    let arg10 = m_bEDIDHead[16]
                    let arg11 = Int(m_bEDIDHead[17]) + 1990
                    buffer = "Manufacturer:\(arg1)\(arg2)\(arg3); Code:\(arg4)\(arg5); Serial:\(arg6) \(arg7) \(arg8) \(arg9); Week:\(arg10), Year:\(arg11)"
                }
                EmitMsgToListMessage(1, buffer)
            }else if type == 1 {
                var buffer:String = ""
                EmitMsgToListMessage(1,"EDID Base(12h-18h):");
                buffer = "Ver:\(m_bEDIDBase[0]).\(m_bEDIDBase[1]); Code:%x%x"
                EmitMsgToListMessage(1, buffer)
            }else if(type == 2){
                var buffer:String = ""
                EmitMsgToListMessage(1,"EDID Timing0(36h-47h):")
                let arg1 = Double(m_bEDIDTiming0[0]+(m_bEDIDTiming0[1]<<8))/100.0
                let arg2 = m_bEDIDTiming0[2]+((m_bEDIDTiming0[4]&0xF0)<<4)
                let arg3 = m_bEDIDTiming0[5]+((m_bEDIDTiming0[7]&0xF0)<<4)
                let arg4 = m_bEDIDTiming0[12]+((m_bEDIDTiming0[14]&0xF0)<<4)
                let arg5 = m_bEDIDTiming0[13]+((m_bEDIDTiming0[14]&0x0F)<<8)
                buffer = "Pixel Clock:\(arg1); HorzActive:\(arg2); VertActive:\(arg3); HorzImgSize:\(arg4)mm; VertImgSize:\(arg5)mm"
                EmitMsgToListMessage(1, buffer)
                
            }else if(type == 3){
                var buffer:String = ""
                EmitMsgToListMessage(1,"EDID Timing1(48h-59h):");
                buffer = TransferEDIDTiming(m_bEDIDTiming1, 18);
                EmitMsgToListMessage(1, buffer)
            }else if(type == 4){
                var buffer:String = ""
                EmitMsgToListMessage(1,"EDID Timing2(5Ah-6Bh):");
                buffer = TransferEDIDTiming(m_bEDIDTiming2, 18);
                EmitMsgToListMessage(1, buffer)
            }else if(type == 5){
                var buffer:String = ""
                EmitMsgToListMessage(1,"EDID Timing3(6Ch-7Dh):");
                buffer = TransferEDIDTiming(m_bEDIDTiming3, 18);
                EmitMsgToListMessage(1, buffer)
            }
        } //main.sync
        return true
    }
    
    func TransferEDIDTiming(_ content:[UInt8], _ count:Int) -> String {
        var buffer:String = ""
        if(count < 18){
            return ""
        }
        if((content[0] != 0) || (content[1] != 0) || (content[2] != 0) || (content[4] != 0)){
            return ""
        }
        
        if(content[3] == 0xFF){//Monitor Serial Number - Stored as ASCII
            buffer = "Monitor Serial Number:"
            for i in 5...17 {
                let u = UnicodeScalar(content[i])
                // Convert UnicodeScalar to a Character.
                let char = Character(u)
                buffer.append(char)
            }
        }else if(content[3] == 0xFE){//ASCII String - Stored as ASCII
            buffer = "ASCII String:"
            for i in 5...17 {
                let u = UnicodeScalar(content[i])
                // Convert UnicodeScalar to a Character.
                let char = Character(u)
                buffer.append(char)
            }
        }else if(content[3] == 0xFD){//Monitor range limits, binary coded
            let arg1 = content[5]
            let arg2 = content[6]
            let arg3 = content[7]
            let arg4 = content[8]
            let arg5 = content[9]
            buffer = "Monitor range limits:Vert[\(arg1)-\(arg2)]Hz; Horz[\(arg3)-\(arg4)]KHz; Clk[\(arg5)]MHz"
        }else if(content[3] == 0xFC){//Monitor name - Stored as ASCII
            buffer = "Monitor name:"
            var i:Int = 5
            while content[i] != 0x0A {
                let u = UnicodeScalar(content[i])
                // Convert UnicodeScalar to a Character.
                let char = Character(u)
                buffer.append(char)
                i += 1
                if i >= 18 {
                    break;
                }
            }
            buffer.append("")
        }else if(content[3] == 0xFB){//Descriptor contains additional color point data
            buffer = "additional color point data: not translated..."
        }else if(content[3] == 0xFA){//Descriptor contains Standard Timing Identifications
            buffer = "Standard Timing Identifications: not translated..."
        }else if((content[3] <= 0xF9) && (content[3] >= 0x11)){//Currently undefined
            buffer = "Currently undefined"
        }else if(content[3] == 0x10){//Dummy descriptor
            buffer = "Dummy descriptor"
        }else{//Descriptor defined by manufacturer
            buffer = "Descriptor defined by manufacturer"
        }
        return buffer
    }
    
    func ShowEDIDResult(){
        var bufferEDIDInfo:String = ""
        var bufferEDIDTimeInfo:String = ""
        let ediinfoArg1 = ((m_bEDIDHead[8]&0x7C)>>2)+65-1  // 65: 'A'
        let ediinfoArg2 = ((m_bEDIDHead[8]&0x3)<<3)+((m_bEDIDHead[9]&0xE0)>>5)+65-1
        let ediinfoArg3 = (m_bEDIDHead[9]&0x1F)+65-1
        let ediinfoArg4 = m_bEDIDHead[11]
        let ediinfoArg5 = m_bEDIDHead[10]
        let ediinfoArg6 = m_bEDIDHead[15]
        let ediinfoArg7 = m_bEDIDHead[14]
        let ediinfoArg8 = m_bEDIDHead[13]
        let ediinfoArg9 = m_bEDIDHead[12]
        let ediinfoArg10 = m_bEDIDHead[16]
        let ediinfoArg11 = Int(m_bEDIDHead[17]) + 1990
        //EDID info
        bufferEDIDInfo = "Manufacturer:\(ediinfoArg1)\(ediinfoArg2)\(ediinfoArg3); Code:\(ediinfoArg4)\(ediinfoArg5); Serial:\(ediinfoArg6) \(ediinfoArg7) \(ediinfoArg8) \(ediinfoArg9); Week:\(ediinfoArg10), Year:\(ediinfoArg11)"
        
        //EDID Timing info
        let edidtimeArg1 = Double(m_bEDIDTiming0[0]+(m_bEDIDTiming0[1]<<8)) / 100.0
        let edidtimeArg2 = m_bEDIDTiming0[2]+((m_bEDIDTiming0[4]&0xF0)<<4)
        let edidtimeArg3 = m_bEDIDTiming0[5]+((m_bEDIDTiming0[7]&0xF0)<<4)
        let edidtimeArg4 = m_bEDIDTiming0[12]+((m_bEDIDTiming0[14]&0xF0)<<4)
        let edidtimeArg5 = m_bEDIDTiming0[13]+((m_bEDIDTiming0[14]&0x0F)<<8)
        bufferEDIDTimeInfo = "Pixel Clock:\(edidtimeArg1)MHz; HorzActive:\(edidtimeArg2); VertActive:\(edidtimeArg3); HorzImgSize:\(edidtimeArg4)mm; VertImgSize:\(edidtimeArg5)mm"
        DispatchQueue.main.sync {
            ledidinfo.stringValue = "EDID info:\(bufferEDIDInfo)"
            ledidtime.stringValue = "EDID info:\(bufferEDIDTimeInfo)"
        }
    }
    func ShowDPResult(){
        var bufferDPgen:String = ""
        var bufferLane0:String = ""
        var bufferLane1:String = ""
        var bufferLane2:String = ""
        var bufferLane3:String = ""

        //DP gen
        let laneArg1 = m_bDPSinkCapability[0]>>4
        let laneArg2 = m_bDPSinkCapability[0]&0xF
        let laneArg3 = (m_bDPSinkCapability[1] == 0x06) ? (1.62) : ((m_bDPSinkCapability[1] == 0x0A) ? (2.7) : ((m_bDPSinkCapability[1] == 0x14) ? (5.4) : (0)))
        let laneArg4 = m_bDPSinkCapability[2]&0x1F
        let laneArg5 = ((m_bDPSinkCapability[3]&0x01) == 0) ? ("No down spread") : ("Up to 0.5% down spread")
        var downstreamport:String = ""
        if m_bDPSinkCapability[5] & 0x01 == 0 {
            downstreamport = "No port"
        }else if m_bDPSinkCapability[5]&0x06 == 0x00 {
            downstreamport = "DisplayPort"
        }else if m_bDPSinkCapability[5]&0x06 == 0x02 {
            downstreamport = "Analog VGA or analog video over DVI-I"
        }else if m_bDPSinkCapability[5]&0x06 == 0x04 {
            downstreamport = "DVI, HDMI or DP++"
        }else{
            downstreamport = "Others"
        }
        bufferDPgen = "REV-\(laneArg1).\(laneArg2); MAX_LINK_RATE-\(laneArg3)Gbps; MAX_LANE_COUNT-\(laneArg4); MAX_DOWNSPREAD-\(laneArg5); DOWNSTREAMPORT-\(downstreamport)"

        //DP lane0
        let lane0Arg1 = ((m_bDPLinkStatus[2]&0x01) > 0) ? ("CR_DONE") : ("N/A")
        let lane0Arg2 = ((m_bDPLinkStatus[2]&0x02) > 0) ? ("CHANNEL_EQ_DONE") : ("N/A")
        let lane0Arg3 = ((m_bDPLinkStatus[2]&0x04) > 0) ? ("SYMBOL_LOCKED") : ("N/A")
        let lane0Arg4 = ((m_bDPLinkErr[1]&0x80) > 0) ? ("valid") : ("invalid")
        let lane0Arg5 = m_bDPLinkErr[0]+((m_bDPLinkErr[1]&0x7F)<<8)
        bufferLane0 = "STATUS-\(lane0Arg1)|\(lane0Arg2)|\(lane0Arg3); SYMBOL_ERROR_COUNT-\(lane0Arg4)|\(lane0Arg5)"

        //DP lane1
        let lane1Arg1 = ((m_bDPLinkStatus[2]&0x10) > 0) ? ("CR_DONE") : ("N/A")
        let lane1Arg2 = ((m_bDPLinkStatus[2]&0x20) > 0) ? ("CHANNEL_EQ_DONE") : ("N/A")
        let lane1Arg3 = ((m_bDPLinkStatus[2]&0x40) > 0) ? ("SYMBOL_LOCKED") : ("N/A")
        let lane1Arg4 = ((m_bDPLinkErr[3]&0x80) > 0) ? ("valid") : ("invalid")
        let lane1Arg5 = m_bDPLinkErr[2]+((m_bDPLinkErr[3]&0x7F)<<8)
        bufferLane1 = "STATUS-\(lane1Arg1)|\(lane1Arg2)|\(lane1Arg3); SYMBOL_ERROR_COUNT-\(lane1Arg4)|\(lane1Arg5)"

        //DP lane2
        let lane2Arg1 = ((m_bDPLinkStatus[3]&0x01) > 0) ? ("CR_DONE") : ("N/A")
        let lane2Arg2 = ((m_bDPLinkStatus[3]&0x02) > 0) ? ("CHANNEL_EQ_DONE") : ("N/A")
        let lane2Arg3 = ((m_bDPLinkStatus[3]&0x04) > 0) ? ("SYMBOL_LOCKED") : ("N/A")
        let lane2Arg4 = ((m_bDPLinkErr[5]&0x80) > 0) ? ("valid") : ("invalid")
        let lane2Arg5 = m_bDPLinkErr[4]+((m_bDPLinkErr[5]&0x7F)<<8)
        bufferLane2 = "STATUS-\(lane2Arg1)|\(lane2Arg2)|\(lane2Arg3); SYMBOL_ERROR_COUNT-\(lane2Arg4)|\(lane2Arg5)"
        
        //DP lane3
        let lane3Arg1 = ((m_bDPLinkStatus[3]&0x10) > 0) ? ("CR_DONE") : ("N/A")
        let lane3Arg2 = ((m_bDPLinkStatus[3]&0x20) > 0) ? ("CHANNEL_EQ_DONE") : ("N/A")
        let lane3Arg3 = ((m_bDPLinkStatus[3]&0x40) > 0) ? ("SYMBOL_LOCKED") : ("N/A")
        let lane3Arg4 = ((m_bDPLinkErr[7]&0x80) > 0) ? ("valid") : ("invalid")
        let lane3Arg5 = m_bDPLinkErr[6]+((m_bDPLinkErr[7]&0x7F)<<8)
        bufferLane3 = "STATUS-\(lane3Arg1)|\(lane3Arg2)|\(lane3Arg3); SYMBOL_ERROR_COUNT-\(lane3Arg4)|\(lane3Arg5)"
        DispatchQueue.main.sync {
            self.ldpgen.stringValue = ("DPCD gen info:\(bufferDPgen)")
            self.ldplane0.stringValue = "DPCD lane0 info:\(bufferLane0)"
            self.ldplane1.stringValue = "DPCD lane1 info:\(bufferLane1)"
            self.ldplane2.stringValue = "DPCD lane2 info:\(bufferLane2)"
            self.ldplane3.stringValue = "DPCD lane3 info:\(bufferLane3)"
        }
    }
    
    func ShowFuncResultInfo(_ type:TestType, _ buffer:String){
        var result:String = ""
        switch(type){
        case .TEST_TYPE_APORT_A:
            result = "A Port Test with A face "
        case .TEST_TYPE_APORT_B:
            result = "B Port Test with B face "
        case .TEST_TYPE_BPORT_A:
            result = "B Port Test with A face "
        case .TEST_TYPE_BPORT_B:
            result = "B Port Test with B face "
        case .TEST_TYPE_PD_A:
            result = "PD Test with A face "
        case .TEST_TYPE_PD_B:
            result = "PD Test with B face "
        case .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_A:
            result = "Auto Test A Port A face "
        case .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_B:
            result = "Auto Test A Port B face "
        case .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_A:
            result = "Auto Test B Port A face "
        case .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_B:
            result = "Auto Test B Port B face "
        default:
            result = "Unknown Test "
        }
        result += buffer
        DispatchQueue.main.sync {
            EmitMsgToListMessage(1, result)
        }
    }
    
    func setColor(measvol:Double) -> Bool{
        if( measvol > 4.6){
            DispatchQueue.main.sync {
                m_bIsVbusDet = true
                m_bIsGetADC  = true
                m_nIsConnectionPassA.insert(1, at: 11)
                ckA12.state = NSOnState
                
                m_nIsConnectionPassB.insert(1, at: 0)
                ckB01.state = NSOnState
                //ckB01->setStyleSheet("background-color:rgb(0, 255, 0)");
                
                m_nIsConnectionPassA.insert(1, at: 8)
                ckA09.state = NSOnState
                //ckA09->setStyleSheet("background-color:rgb(0, 255, 0)");
                
                m_nIsConnectionPassB.insert(1, at: 3)
                ckB04.state = NSOnState
                //ckB04->setStyleSheet("background-color:rgb(0, 255, 0)");
                
                m_nIsConnectionPassA[3] = 1;
                ckA04.state = NSOnState
                //ckA04->setStyleSheet("background-color:rgb(0, 255, 0)");
                
                m_nIsConnectionPassB[8] = 1;
                ckB09.state = NSOnState
                //ckB09->setStyleSheet("background-color:rgb(0, 255, 0)");
                
                m_nIsConnectionPassA[0] = 1;
                ckA01.state = NSOnState
                
                m_nIsConnectionPassB[11] = 1;
                ckB12.state = NSOnState
                //ui.ckb12->setStyleSheet("background-color:rgb(0, 255, 0)");
                
                if((m_nTestType == .TEST_TYPE_APORT_A) ||
                    (m_nTestType == .TEST_TYPE_BPORT_A) ||
                    ((m_nTestType == .TEST_TYPE_AUTO) &&
                        ((m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_A) ||
                            (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_A)))){
                    m_nIsConnectionPassA[4] = 1;
                    ckA05.state = NSOnState
                    //ckA05->setStyleSheet("background-color:rgb(0, 255, 0)");
                }else if((m_nTestType == .TEST_TYPE_APORT_B) ||
                    (m_nTestType == .TEST_TYPE_BPORT_B) ||
                    ((m_nTestType == .TEST_TYPE_AUTO) &&
                        ((m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_B) ||
                            (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_B)))){
                    m_nIsConnectionPassB[4] = 1;
                    ckB05.state = NSOnState
                    //ckB05->setStyleSheet("background-color:rgb(0, 255, 0)");
                }
            }
            if(m_bIsDispConnected){
                if(m_bIsEDIDGotten){
                    m_bIsTestSingPass = true;
                }
            }else{
                if(m_bIsDPCDGotten){
                    m_bIsTestSingPass = true;
                }
            }
            
            if(measvol > 5){
                if((m_nTestType == .TEST_TYPE_APORT_A) ||
                    (m_nTestType == .TEST_TYPE_APORT_A) ||
                    (m_nTestType == .TEST_TYPE_BPORT_A) ||
                    (m_nTestType == .TEST_TYPE_BPORT_B)){
                    m_bIsTestSingPass = true;
                }
                else if((m_nTestType == .TEST_TYPE_AUTO) &&
                    ((m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_A) ||
                        (m_nTestAutoType == .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_B) ||
                        (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_A) ||
                        (m_nTestAutoType == .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_B)))
                    {
                    m_bIsTestAutoPDPass = true;
                }
            }
            m_nTestSingCount += 1
            m_nTestAutoCount += 1
        }else{
            m_bIsVbusDet = false
            m_bIsGetADC  = false
            ShowFuncResultInfo(m_nTestType,"fail");
            return false
        }
        return true
    }
    
    @IBAction func fnAATest(_ sender: Any) {
        if !CheckTestItems() {
            return
        }
        InitLogFile(false);
        ResetResult(true)
        InitTest(.TEST_TYPE_APORT_A)
        m_bIsTestStart = true
        switchProgressTimer(true)
        DispatchQueue.global().async {
            _ = self.OnePortOneFaceTest(testtype: .TEST_TYPE_APORT_A)
        }
        
    }
    @IBAction func fnABTest(_ sender: Any) {
        if !CheckTestItems() {
            return
        }
        InitLogFile(false);
        ResetResult(true)
        InitTest(.TEST_TYPE_APORT_B)
        m_bIsTestStart = true
        switchProgressTimer(true)
        DispatchQueue.global().async {
            _ = self.OnePortOneFaceTest(testtype: .TEST_TYPE_APORT_B)
        }
    }
    @IBAction func fnBATest(_ sender: Any) {
        if !CheckTestItems() {
            return
        }
        InitLogFile(false);
        ResetResult(true)
        InitTest(.TEST_TYPE_BPORT_A)
        m_bIsTestStart = true
        switchProgressTimer(true)
        DispatchQueue.global().async {
            _ = self.OnePortOneFaceTest(testtype: .TEST_TYPE_BPORT_A)
        }
    }
    @IBAction func fnBBTest(_ sender: Any) {
        if !CheckTestItems() {
            return
        }
        InitLogFile(false);
        ResetResult(true)
        InitTest(.TEST_TYPE_BPORT_B)
        m_bIsTestStart = true
        switchProgressTimer(true)
        DispatchQueue.global().async {
            _ = self.OnePortOneFaceTest(testtype: .TEST_TYPE_BPORT_B)
        }
        
        //ckA01.image = NSImage.swatchWithColor(color: .green, size: NSMakeSize(10, 10))
    }
    
    @IBOutlet weak var ckAAPD: NSButton!
    @IBOutlet weak var ckABPD: NSButton!
    @IBOutlet weak var ckBAPD: NSButton!
    @IBOutlet weak var ckBBPD: NSButton!
    func PDTestBackThread(){
        //First of all, Judge the PD testing or not, if PD test now, then kill pdtimer to
        //stop PD test, otherwise we create a pdtimer to start pd test
        if(m_bPDTesting){
            m_bPDTesting = false
            DispatchQueue.main.sync {
            switchPDTimer(state: false)
            }
        }else{
            var testType:Commands = .TYPEA_A_GO
            var aapd:Bool = false
            var abpd:Bool = false
            var bapd:Bool = false
            var bbpd:Bool = false
            
            DispatchQueue.main.sync {
            aapd = ckAAPD.state == NSOnState
            abpd = ckABPD.state == NSOnState
            bapd = ckBAPD.state == NSOnState
            bbpd = ckBBPD.state == NSOnState
            }
            if !(aapd || abpd || bapd || bbpd){
                mlogger?.error("No Port Face Selected of PD Test")
                return
            }
            //First of all, switch control signals according to the selected
            if(aapd){
                testType = .TYPEA_A_GO
            }
            if(abpd){
                testType = .TYPEA_B_GO
            }
            if(bapd){
                testType = .TYPEB_A_GO
            }
            if(bbpd){
                testType = .TYPEB_B_GO
            }
            
            _ = self.sendRecv(testType.rawValue, false)
//            usleep(useconds_t(TWOSEND))
//            _ = self.sendRecv(testType.rawValue, false)
            
            PDAutoTestBackThread()
            //Create pdtimer to start PD Test
            DispatchQueue.main.sync {
                m_bPDTesting = true
                self.switchPDTimer(state: true)
            }
        }
    }
    @IBAction func fnPDTest(_ sender: Any) {
        if !CheckTestItems() {
            return
        }
        if !m_bPDTesting {
            InitLogFile(false)
        }
        ResetResult(true)
        listResult.string = ""
        InitTest(.TEST_TYPE_NULL)
        DispatchQueue.global().async {
            self.PDTestBackThread()
        }
    }
    func PDAutoTestBackThread(){
        //Get VBUS voltage to decide whether CC lane is OK
        print("here in PDAutoTest")
        let sendcmd:Commands = .TYPEV_I_GO
        var byteArray = sendRecv(sendcmd.rawValue);
        if m_isGlobalStop || byteArray.count == 0 {
            return
        }
        //Translate HEX to decimal value
        var tempU16: [UInt8] = [byteArray[1], byteArray[0]]
        let vs = UnsafePointer(tempU16).withMemoryRebound(to: UInt16.self, capacity: 1) { $0.pointee }
        tempU16 = [byteArray[3], byteArray[2]]
        let vm = UnsafePointer(tempU16).withMemoryRebound(to: UInt16.self, capacity: 1) { $0.pointee }
        tempU16 = [byteArray[5], byteArray[4]]
        let im = UnsafePointer(tempU16).withMemoryRebound(to: UInt16.self, capacity: 1) { $0.pointee }
        mlogger?.error("vs: \(vs), vm: \(vm), im: \(im)")
        let tempstr1:String = String(format: "%02X%02x", byteArray[1], byteArray[0])
        let tempstr2:String = String(format: "%02X%02x", byteArray[3], byteArray[2])
        let tempstr3:String = String(format: "%02X%02x", byteArray[5], byteArray[4])
        DispatchQueue.main.sync {
            EmitMsgToListMessage(1,tempstr1)
            EmitMsgToListMessage(1,tempstr2)
            EmitMsgToListMessage(1,tempstr3)
        }
        if vs == 0 {
            return
        }
        var measvol:Double = 0
        var meascurr:Double = 0
        measvol = 10.09 * 1.2 * Double(vm) / Double(vs)
        //meascurr = (1.2 * Double(im) / Double(vs) - 2.46) / 0.185
        meascurr = (2.4 * Double(im) / Double(vs))
        if((meascurr>1.0)&&(meascurr<2.0)){
            meascurr = meascurr + MINCOMPENSATION;
        }
        if(meascurr>2.0){
            meascurr = meascurr + MAXCOMPENSATION;
        }
        let volStr = NSString.init(format: "%.2f", measvol)
        let currStr = NSString.init(format:"%.2f", meascurr)
        DispatchQueue.main.sync {
            var strmsg:String = "Charging Results: VBUS:\(volStr)V, CURR:\(currStr)A"
            EmitMsgToListMessage(1,strmsg);
            
            //update the value to debug ui
            strmsg = "VBUS:\(volStr) V"
            lvbus.stringValue = strmsg
            strmsg = "ISENS:\(currStr) A"
            lisens.stringValue = strmsg
        }
    }
    func PDAutoTest(){
        DispatchQueue.global().async {
            self.PDAutoTestBackThread()
        }
    }
    
    func autoTestBackthread(){
        //let bLoop = ckLoop.state == NSOnState
        var  loopcount:Int = 1
        var abport:Bool = false
        var aport:Bool = false
        var bport:Bool = false
        DispatchQueue.main.sync {
            loopcount = Int(spinLoop.stringValue)!
        }
        for _ in 1...loopcount {
            if m_isGlobalStop {
                return
            }
            DispatchQueue.main.sync {
            let bResult = CheckTestItems();
            if(!bResult){
                return;
            }
            
            m_bIsLogOnlyOne = cklogonlyone.state == NSOnState
            if(m_bIsLogOnlyOne){
                DelExistLog();
            }
            //Init log file
            InitLogFile(false);
            m_nTestTimes = 0;
            m_nTestAutoCount=0;
            
            switch statePortSetting {
            case "AAUTO":
                aport = true
            case "BAUTO":
                bport = true
            case "ABAUTO":
                abport = true
            default:
                abport = true
            }
            
            if (abport || aport || bport) == false {
                mlogger?.error("No Port Selected")
                return;
            }
            
            if(abport){
                progressBar.maxValue = Double(AUTOSIGCOUNT)
                progressBar.minValue = 0
            }else {
                progressBar.maxValue = Double(AUTOSUBCOUNT)
                progressBar.minValue = 0
            }
            
            //Create Test timer and protimer
            switchTestTime(true)
            switchProgressTimer(true)
            } //main.sync
            //Auto test according to the selected items
            if(abport){
                //First of all, Reset the result
                DispatchQueue.main.sync {
                ResetResult(true);
                InitTest(.TEST_TYPE_AUTO, .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_A);
                }
                m_bIsTestStart = true;
                _ = OnePortOneFaceTest(testtype: .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_A);
                
                //A PORT B Face test
                DispatchQueue.main.sync {
                ResetResult(true);
                InitTest(.TEST_TYPE_AUTO, .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_B);
                }
                m_bIsTestStart = true;
                _ = OnePortOneFaceTest(testtype: .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_B);
                
                //B Port A Face Test
                DispatchQueue.main.sync {
                ResetResult(true);
                InitTest(.TEST_TYPE_AUTO, .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_A);
                }
                m_bIsTestStart = true;
                _ = OnePortOneFaceTest(testtype: .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_A);
                
                //B Port B Face Test
                DispatchQueue.main.sync {
                ResetResult(true);
                InitTest(.TEST_TYPE_AUTO, .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_B);
                }
                m_bIsTestStart = true;
                _ = OnePortOneFaceTest(testtype: .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_B);
            }
            
            if(aport){
                DispatchQueue.main.sync {
                //First of all, Reset the result
                ResetResult(true);
                InitTest(.TEST_TYPE_AUTO, .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_A);
                }
                m_bIsTestStart = true;
                _ = OnePortOneFaceTest(testtype: .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_A);
                
                //A PORT B Face test
                DispatchQueue.main.sync {
                ResetResult(true);
                InitTest(.TEST_TYPE_AUTO, .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_B);
                }
                m_bIsTestStart = true;
                _ = OnePortOneFaceTest(testtype: .TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_B);
                
                if(m_bIsLogOnlyOne){
                    if(m_bAFacePass && m_bBFacePass){
                        CreateResLogFile(true);
                    }else{
                        CreateResLogFile(false);
                    }
                }
            }
            
            if(bport){
                DispatchQueue.main.sync {
                //B Port A Face Test
                ResetResult(true);
                InitTest(.TEST_TYPE_AUTO, .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_A);
                }
                m_bIsTestStart = true;
                _ = OnePortOneFaceTest(testtype: .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_A);
                
                //B Port B Face Test
                DispatchQueue.main.sync {
                ResetResult(true);
                InitTest(.TEST_TYPE_AUTO, .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_B);
                }
                m_bIsTestStart = true;
                _ = OnePortOneFaceTest(testtype: .TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_B);
                
                if(m_bIsLogOnlyOne){
                    if(m_bAFacePass && m_bBFacePass){
                        CreateResLogFile(true);
                    }else{
                        CreateResLogFile(false);
                    }
                }
            }
            DispatchQueue.main.sync {
            //Stop testtimer and protimer
            switchTestTime(false)
            switchProgressTimer(false)
            }
            //Delete Log file when auto test pass
            InitLogFile(true);
        }//for loop
    }
    @IBAction func fnAutoTest(_ sender: Any) {
        if !CheckTestItems() {
            return
        }

        InitLogFile(false);
        ResetResult(true)
        listResult.string = ""
        InitTest(.TEST_TYPE_AUTO)
        DispatchQueue.global().async {
            self.autoTestBackthread()
        }
    }
    func CreateResLogFile(_ Pass:Bool){
        
    }
    func DelExistLog(){

    }

    func sendRecv(_ message: String, _ bfmt:Bool = true) -> [UInt8]{
        var recvArray:[UInt8] = []
        do {
            usleep(200000)
            _ = try serialPort.writeString(message)
            mlogger?.error("Send Data : \(message)")
            if message == Commands.TYPEF_T_GO.rawValue {
                usleep(useconds_t(USBDELAY))
            }
            if message == Commands.TYPEA_A_GO.rawValue || message == Commands.TYPEA_B_GO.rawValue || message == Commands.TYPEB_A_GO.rawValue || message == Commands.TYPEB_B_GO.rawValue {
                usleep(3000000)
            }else{
                usleep(100000)
            }

            var prepareEndFlag:Int = 0
            while true {
                let byte:UInt8 = try serialPort.readByte()
                mlogger?.debug("received byte: \(byte)")
                if byte == 13 {
                    prepareEndFlag = 1
                }else if byte == 10 {
                    if prepareEndFlag == 1 {
                        _ = recvArray.popLast()
                        break
                    }else{
                        prepareEndFlag = 0
                    }
                }else {
                    prepareEndFlag = 0
                }
                recvArray.append(byte)
            }
        } catch PortError.fackTimeout {
            mlogger?.error("fackTimeout as end data")
        } catch {
            mlogger?.error("Error: \(error)")
        }
        if bfmt {
            var hexString:String = ""
            for value in recvArray{
                hexString += String(format:"0x%2X ", value)
            }
            mlogger?.error("(len:\(recvArray.count)) Read Data = \(hexString)")
        }else{
            var asciiString:String = ""
            for value in recvArray{
                asciiString.append(Int2Char(value))
            }
            mlogger?.error("(len:\(recvArray.count)) Read Data = \(asciiString)")
        }
        return recvArray
    }
    
    func EmitMsgToListMessage(_ num:Int, _ Msg:String) {
        let endMsg = Msg + "\n"
        switch num {
        case 1:
            listResult.isEditable = true
            listResult.scrollToEndOfDocument(listResult.self)
            let range = listResult.selectedRange()
            listResult.insertText(endMsg, replacementRange: range)
            listResult.isEditable = false
        case 2:
            listRecv.isEditable = true
            listRecv.scrollToEndOfDocument(listRecv.self)
            let range = listRecv.selectedRange()
            listRecv.insertText(endMsg, replacementRange: range)
            listRecv.isEditable = false
        default:
            listResult.isEditable = true
            listResult.scrollToEndOfDocument(listResult.self)
            let range = listResult.selectedRange()
            listResult.insertText(endMsg, replacementRange: range)
            listResult.isEditable = false
        }
        //TODO: Write the data to log file
        mlogger?.error(Msg)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        switchWidgets(state: false)
        timerProgress = Timer.scheduledTimer(
            timeInterval: 0.5,
            target: self,
            selector: #selector(updateProgress),
            userInfo: nil,
            repeats: true
        )
        timerProgress.invalidate()
        pdtimer = Timer.scheduledTimer(
            timeInterval: TimeInterval(PDINTERVAL),
            target: self,
            selector: #selector(PDAutoTest),
            userInfo: nil,
            repeats: true
        )
        pdtimer.invalidate()
        testtimer = Timer.scheduledTimer(
            timeInterval: 1.0,
            target: self,
            selector: #selector(ShowTestTime),
            userInfo: nil,
            repeats: true
        )
        testtimer.invalidate()
    }
    override var representedObject: Any? {
        didSet {
            // Update the view, if already loaded.
        }
    }
    
    func InitTest(_ type:TestType, _ autotype:TestType = .TEST_AUTO_TYPE_NULL ) {
        m_nTestSingCount = 0
        m_bIsTestSingPass = false
        m_nTestAutoSubCount = 0
        
        if(type != .TEST_TYPE_AUTO){
            m_nTestTimes     = 0;
            m_nTestAutoCount = 0;
        }
        
        m_bIsTestAutoPass = false
        m_bIsTestAutoPDPass = false
        m_bIsTestAutoUSB2Pass = false
        m_bIsTestAutoDPPass = false
        m_bIsTestStart = false
        m_bIsGetDPCD = false
        m_bIsGetEDID = false
        m_bIsGetADC = false
        m_bIsVbusDet = false
        m_nTestType = type;
        m_nAutoMode = .TEST_AUTO_MODE_NULL
        m_nTestAutoType = autotype
        if(m_nTestType == .TEST_TYPE_AUTO){
            if(cbitem.selectedTag() == 0){
                m_nAutoMode = .TEST_AUTO_MODE_DP4L_USB2_PD
            }else{
                m_nAutoMode = .TEST_AUTO_MODE_DP4L_USB2
            }
        }
        
        if(m_bAFaceTest && m_bBFaceTest){
            for i in 0...11{
                m_nIsConnectionPassA[i] = 0;
                m_nIsConnectionPassB[i] = 0;
            }
            showConnection(false);

            m_bAFaceTest = false;
            m_bBFaceTest = false;
        }
        if(m_bAFacePass && m_bBFacePass){
            m_bAFacePass = false;
            m_bBFacePass = false;
        }

        lTest.stringValue = "TEST"
        lvbus.stringValue = "VBUS: 0.0V"
        lisens.stringValue = "ISENS: 0.0A"
        lvcc1.stringValue = "VCC1: 0.0V"
        lvcc2.stringValue = "VCC2: 0.0V"
        lusb20.stringValue = "USB2.0: DisConnected"
        ldisplayers.stringValue = "Displayer: Unplugged"
        ldpgen.stringValue = "DPCD gen info: N/A"
        ldplane0.stringValue = "DPCD lane0 info: N/A"
        ldplane1.stringValue = "DPCD lane1 info: N/A"
        ldplane2.stringValue = "DPCD lane2 info: N/A"

        ldplane3.stringValue = "DPCD lane3 info: N/A"
        ledidinfo.stringValue = "EDID info: N/A"
        ledidtime.stringValue = "EDID Timing info: N/A"
        if(type != .TEST_TYPE_AUTO){
            progressBar.doubleValue = 0
        }
    }
    func showConnection(_ bsel:Bool){
        if !bsel{
            ckA01.state = NSOffState
            ckA02.state = NSOffState
            ckA03.state = NSOffState
            ckA04.state = NSOffState
            ckA05.state = NSOffState
            ckA06.state = NSOffState
            ckA07.state = NSOffState
            ckA08.state = NSOffState
            ckA09.state = NSOffState
            ckA10.state = NSOffState
            ckA11.state = NSOffState
            ckA12.state = NSOffState
            
            ckB01.state = NSOffState
            ckB02.state = NSOffState
            ckB03.state = NSOffState
            ckB04.state = NSOffState
            ckB05.state = NSOffState
            ckB06.state = NSOffState
            ckB07.state = NSOffState
            ckB08.state = NSOffState
            ckB09.state = NSOffState
            ckB10.state = NSOffState
            ckB11.state = NSOffState
            ckB12.state = NSOffState
        }
    }
    func switchWidgets(state: Bool){
        if state {
            btnAATest.isEnabled = true
            btnABTest.isEnabled = true
            btnABTest.isEnabled = true
            btnBATest.isEnabled = true
            btnBBTest.isEnabled = true
            btnPDTest.isEnabled = true
            btnAutoTest.isEnabled = true
        }else{
            btnAATest.isEnabled = false
            btnABTest.isEnabled = false
            btnABTest.isEnabled = false
            btnBATest.isEnabled = false
            btnBBTest.isEnabled = false
            btnPDTest.isEnabled = false
            btnAutoTest.isEnabled = false
        }
    }
    
    func ShowTestTime() {
        m_nTestTimes += 1
        leedit.stringValue = String(m_nTestTimes)
        if((m_nTestTimes % 3)==0){
            lTest.stringValue = "Test."
        }else if((m_nTestTimes % 3)==1){
            lTest.stringValue = "Test.."
        }else{
            lTest.stringValue = "Test..."
        }
    }
    
    func ShowProgress(){
        progressBar.doubleValue = Double(m_nTestSingCount)
    }

    func ShowAutoProgress(){
        progressBar.doubleValue = Double(m_nTestAutoCount)
    }
    
    func CheckTestItems() -> Bool {
        m_bIsPDTest = (ckPD.state == NSOnState)
        m_bIsDPTest = (ckDP.state == NSOnState)
        m_bIsUSB2Test = (ckUSB20.state == NSOnState)
        //m_bIsUSB3Test = ui.ckUSB3->isChecked();
        //m_bIsAudioTest= ui.ckAudio->isChecked();
        
        if ((m_bIsPDTest || m_bIsDPTest || m_bIsUSB2Test) == false){
            mlogger?.error("Warning No Test Items Selected")
            return false;
        } else {
            return true;
        }
    }
    func updateProgress(){
        DispatchQueue.main.async {
            self.progressBar.doubleValue = Double(m_nTestSingCount)
        }
    }
    func clearTextList(){
        DispatchQueue.main.async {
            self.listResult.string = ""
            self.listRecv.string = ""
        }
    }
    func switchProgressTimer(_ state:Bool) {
        if state == true{
            if !timerProgress.isValid {
                timerProgress = Timer.scheduledTimer(
                    timeInterval: 0.5,
                    target: self,
                    selector: #selector(updateProgress),
                    userInfo: nil,
                    repeats: true
                )
                RunLoop.main.add(timerProgress, forMode: RunLoopMode.commonModes)
            }
        }else{
            if timerProgress.isValid {
                timerProgress.invalidate()
            }
        }
    }
    func switchTestTime(_ state:Bool){
        if state == true{
            if !testtimer.isValid {
                testtimer = Timer.scheduledTimer(
                    timeInterval: 1.0,
                    target: self,
                    selector: #selector(ShowTestTime),
                    userInfo: nil,
                    repeats: true
                )
                RunLoop.main.add(testtimer, forMode: RunLoopMode.commonModes)
            }
        }else{
            if testtimer.isValid {
                testtimer.invalidate()
            }
        }
    }
    func switchPDTimer(state:Bool){
        if state {
            if !pdtimer.isValid {
                print("turn on pdtimer")
                pdtimer = Timer.scheduledTimer(
                    timeInterval: TimeInterval(PDINTERVAL),
                    target: self,
                    selector: #selector(PDAutoTest),
                    userInfo: nil,
                    repeats: true
                )
                RunLoop.main.add(timerProgress, forMode: RunLoopMode.commonModes)
            }else{
                print("skip turn on pdtimer")
            }
        }else{
            if(pdtimer.isValid){
                pdtimer.invalidate()
            }else{
                print("skip turn off pdtimer")
            }
        }
    }
    func delay(delay:Double) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
            //self.progressBar.increment(by: 2)
            self.progressBar.doubleValue = Double(m_nTestSingCount)
        }
    }
    func ResetResult(_ bAllReset:Bool){
        if bAllReset {
            m_bIsVbusDet = false;
            m_bIsUSB2Connected = false;
            m_bIsDispConnected = false;
            m_bIsDPCDGotten = false;
            m_bIsEDIDGotten = false;
        }
    
        for i in 0...15 {
            m_bDPSinkCapability[i] = 0;
        }
        for i in 0...15 {
            m_bDPLinkField[i] = 0;
        }
        for i in 0...7 {
            m_bDPLinkStatus[i] = 0;
            m_bDPLinkErr[i] = 0;
        }
        for i in 0...17 {
            m_bEDIDHead[i] = 0;
        }
        for i in 0...6 {
            m_bEDIDBase[i] = 0;
        }
        for i in 0...17 {
            m_bEDIDTiming0[i] = 0;
            m_bEDIDTiming1[i] = 0;
            m_bEDIDTiming2[i] = 0;
            m_bEDIDTiming3[i] = 0;
        }
    }
    func InitLogFile(_ bdel: Bool){
        if !m_isGlobalStop {
            mlogger = MLogger()
            mlogger?.debug("This message will go to the console");
            mlogger?.error("Start Logging --");
        }
    }
    @IBAction func fnClearUART(_ sender: Any) {
        listRecv.string = ""
    }
    @IBAction func fnSendUART(_ sender: Any) {
        let cmdMsg:String = txtUartCMD.stringValue
        DispatchQueue.global().async {
            var hexString:String = ""
            let recv:[uint8] = self.sendRecv(cmdMsg, false)
            for value in recv{
                hexString += String(format:"0x%2X ", value)
            }
            DispatchQueue.main.sync {
                self.EmitMsgToListMessage(2, hexString)
            }
        }
    }
}

