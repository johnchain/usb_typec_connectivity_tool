//
//  SerialBoardController.swift
//  RequestResponseDemo
//
//  Created by Andrew Madsen on 3/14/15.
//  Copyright (c) 2015 Open Reel Software. All rights reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a
//	copy of this software and associated documentation files (the
//	"Software"), to deal in the Software without restriction, including
//	without limitation the rights to use, copy, modify, merge, publish,
//	distribute, sublicense, and/or sell copies of the Software, and to
//	permit persons to whom the Software is furnished to do so, subject to
//	the following conditions:
//	
//	The above copyright notice and this permission notice shall be included
//	in all copies or substantial portions of the Software.
//	
//	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
//	OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
//	MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
//	IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
//	CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
//	TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import Cocoa
import ORSSerial

let kTimeoutDuration = 0.5

class SerialBoardController: NSObject, ORSSerialPortDelegate {
	
	enum SerialBoardRequestType: Int {
		case readTemperature = 1
		case readLED
		case setLED
	}
	
	// MARK: - Private
	func pollingTimerFired(_ timer: Timer) {
        //TODO:
	}
	
	// MARK: - ORSSerialPortDelegate
	
    func serialPortWasRemoved(fromSystem serialPort: ORSSerialPort) {
        print("serialPortWasRemoved")
		self.serialPort = nil
	}
	
	func serialPort(_ serialPort: ORSSerialPort, didEncounterError error: Error) {
		print("Serial port \(serialPort) encountered an error: \(error)")
	}
	
	func serialPort(_ serialPort: ORSSerialPort, didReceiveResponse responseData: Data, to request: ORSSerialRequest) {
        let dataAsString = NSString(data: responseData, encoding: String.Encoding.ascii.rawValue)!
        print("serialPort: didReceiveResponse:\(dataAsString)")
		let requestType = SerialBoardRequestType(rawValue: request.userInfo as! Int)!
		switch requestType {
		case .readTemperature, .readLED, .setLED:
            break
		}
	}
	
    func serialPortOpen(){
        print("serialPortOpen")
        self.serialPort?.open()
    }
    
	func serialPortWasOpened(_ serialPort: ORSSerialPort) {
        print("serialPortWasOpened")
//        self.pollingTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(SerialBoardController.pollingTimerFired(_:)), userInfo: nil, repeats: true)
//        self.pollingTimer!.fire()
	}
	
    func serialPortClose(){
        print("serialPortClose")
        self.serialPort?.close()
    }
	func serialPortWasClosed(_ serialPort: ORSSerialPort) {
        print("serialPortWasClosed")
//        self.pollingTimer = nil
	}
	
	// MARK: - Properties
	fileprivate(set) internal var serialPort: ORSSerialPort? {
		willSet {
			if let port = serialPort {
				port.close()
				port.delegate = nil
			}
		}
		didSet {
			if let port = serialPort {
				//port.baudRate = 57600
				port.delegate = self
				port.rts = true
				//port.open()
			}
		}
	}

	fileprivate var pollingTimer: Timer? {
		willSet {
			if let timer = pollingTimer {
				timer.invalidate()
			}
		}
	}
}
