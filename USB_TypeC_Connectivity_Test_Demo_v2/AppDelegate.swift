//
//  AppDelegate.swift
//  USB_TypeC_Connectivity_Test_Demo_v2
//
//  Created by johnchain on 18/03/2018.
//  Copyright © 2018 J. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

