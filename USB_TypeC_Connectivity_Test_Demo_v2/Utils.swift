//
//  Utils.swift
//  USB_TypeC_Connectivity_Test_Demo_v2
//
//  Created by johnchain on 22/03/2018.
//  Copyright © 2018 J. All rights reserved.
//

import Foundation
import Cocoa
import Log4swift

var mlogger: MLogger?

let PDINTERVAL:Int = 30
//#define SWITTIME     5000
//#define DELAYTIME    200
let USBDELAY:Int        = 500000
let TWOSEND:Int         = 1000000
//#define SIGCOUNT     26
let AUTOSIGCOUNT:Int = 114
let AUTOSUBCOUNT:Int = 52
let USLEEP_INTERVEL = 500000
let MAXCOMPENSATION:Double = 0.0
let MINCOMPENSATION:Double = 0.0

var m_isGlobalStop = false
var bConnectFlag = false
var bLogInit = false
var bEDID = false
var m_bAFacePass = false
var m_bBFacePass = false
var m_bAFaceTest = false
var m_bBFaceTest = false
var m_bPDTesting = false
var m_bIsDPTest = false
var m_bIsPDTest = false
var m_bIsUSB2Test = false
var m_bIsUSB3Test = false
var m_bIsAudioTest = false
var m_bIsThunderboltTest = false
var m_bIsAPortPass = false
var m_bIsBPortPass = false
var m_bIsLogOnlyOne = false
var m_globalpath:String = ""
var m_LogPath:String = ""

var m_bIsUSB2Connected = false
var m_bIsDispConnected = false
var m_bIsDPCDGotten = false
var m_bIsEDIDGotten = false
var m_bIsTestAutoPass = false
var m_bIsTestAutoPDPass = false
var m_bIsTestAutoUSB2Pass = false
var m_bIsTestAutoDPPass = false
var m_bIsVbusDet = false
var m_bIsTestStart = false
var m_bChkDebug = false
var m_bDPSinkCapability = [UInt8](repeatElement(0, count: 16))
var m_bDPLinkField = [UInt8](repeatElement(0, count: 16))  // 15 len maybe wrong
var m_bDPLinkStatus = [UInt8](repeatElement(0, count: 8))
var m_bDPLinkErr = [UInt8](repeatElement(0, count: 8))
var m_bEDIDHead = [UInt8](repeatElement(0, count: 18))
var m_bEDIDBase = [UInt8](repeatElement(0, count: 7))
var m_bEDIDTiming0 = [UInt8](repeatElement(0, count: 18))
var m_bEDIDTiming1 = [UInt8](repeatElement(0, count: 18))
var m_bEDIDTiming2 = [UInt8](repeatElement(0, count: 18))
var m_bEDIDTiming3 = [UInt8](repeatElement(0, count: 18))
var m_nIsConnectionPassA = [UInt8](repeatElement(0, count: 12))  //0: not tested; 1: passed; 2: failed
var m_nIsConnectionPassB = [UInt8](repeatElement(0, count: 12))  //0: not tested; 1: passed; 2: failed
var m_bIsGetDPCD:Bool = false
var m_bIsGetEDID:Bool = false
var m_bIsGetADC:Bool = false
var m_nTestType:TestType = .TEST_TYPE_NULL
var m_nTestSingCount:Int = 0
var m_bIsTestSingPass = false
var m_nTestAutoType:TestType = .TEST_AUTO_TYPE_NULL
var m_nTestAutoCount:Int = 0
var m_nTestAutoSubCount:Int = 0
var m_nAutoTimes = 0
var m_nAutoMode:AUTOMode = .TEST_AUTO_MODE_NULL
var m_nTestTimes: Int = 0
//COLORREF m_crStFuncResult;

enum Commands:String{
    case TYPEA_A_GO = "TYPEA_A_GO\r\n"
    case TYPEA_B_GO = "TYPEA_B_GO\r\n"
    case TYPEB_A_GO = "TYPEB_A_GO\r\n"
    case TYPEB_B_GO = "TYPEB_B_GO\r\n"
    case TYPEP_S_GO = "TYPEP_S_GO\r\n"
    case TYPEV_I_GO = "TYPEV_I_GO\r\n"
    case TYPEF_T_GO = "TYPEF_T_GO\r\n"
    case TYPED_D_GO = "TYPED_D_GO\r\n"
}

enum TestType {
    case TEST_TYPE_NULL
    case TEST_TYPE_APORT_A
    case TEST_TYPE_APORT_B
    case TEST_TYPE_BPORT_A
    case TEST_TYPE_BPORT_B
    case TEST_TYPE_DP4L_A
    case TEST_TYPE_DP4L_B
    case TEST_TYPE_PD_A
    case TEST_TYPE_PD_B
    case TEST_TYPE_AUTO
    case TEST_PORT_A
    case TEST_PORT_B
    case TEST_TYPE_MAX
    case TEST_AUTO_TYPE_NULL
    case TEST_AUTO_TYPE_APORT_DP4L_USB2_A
    case TEST_AUTO_TYPE_APORT_DP4L_USB2_B
    case TEST_AUTO_TYPE_BPORT_DP4L_USB2_A
    case TEST_AUTO_TYPE_BPORT_DP4L_USB2_B
    case TEST_AUTO_TYPE_APORT_PD_A
    case TEST_AUTO_TYPE_APORT_PD_B
    case TEST_AUTO_TYPE_BPORT_PD_A
    case TEST_AUTO_TYPE_BPORT_PD_B
    case TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_A
    case TEST_AUTO_TYPE_APORT_DP4L_USB2_PD_B
    case TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_A
    case TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD_B
    case TEST_AUTO_TYPE_APORT_USB2_A
    case TEST_AUTO_TYPE_APORT_USB2_B
    case TEST_AUTO_TYPE_BPORT_USB2_A
    case TEST_AUTO_TYPE_BPORT_USB2_B
    case TEST_AUTO_TYPE_APORT_DP4L_USB2_PD
    case TEST_AUTO_TYPE_BPORT_DP4L_USB2_PD
    case TEST_AUTO_TYPE_MAX
}

enum AUTOMode{
    case TEST_AUTO_MODE_NULL
    case TEST_AUTO_MODE_DP4L_USB2
    case TEST_AUTO_MODE_DP4L_USB2_PD
    case TEST_AUTO_MODE_MAX
}


extension NSImage {
    class func swatchWithColor(color: NSColor, size: NSSize) -> NSImage {
        let image = NSImage(size: size)
        image.lockFocus()
        color.drawSwatch(in: NSMakeRect(0, 0, size.width, size.height))
        image.unlockFocus()
        return image
    }
}

func Int2Char(_ input:UInt8) -> Character{
    let u = UnicodeScalar(input)
    // Convert UnicodeScalar to a Character.
    return Character(u)
}
func Char2Int(_ input: Character) -> UInt8{
    
    return 0
}

class MLogger {
    var logger: Logger!
    var date: Date!
    let formatter = DateFormatter()
    
    func InitLogFile(_ bdel: Bool){
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMddHHmmss"
        let result = formatter.string(from: date)
        
        logger = Logger.getLogger("test.logger");
        let stdOutAppender = StdOutAppender("console");
        let path = NSHomeDirectory() + "/Documents/TypeCLog/"
        let fileName = result + ".log"
        let fileAppender = FileAppender(identifier: "errorFile", filePath: path + fileName);
        
        stdOutAppender.thresholdLevel = .Debug;
        fileAppender.thresholdLevel = .Error;
        logger.appenders = [stdOutAppender, fileAppender];
        
//        logger.debug ("This message will go to the console");
//        logger.error ("Start Logging -- \(path)\(fileName)");
    }
    
    init() {
        formatter.dateFormat = "[yyyy-MM-dd HH-mm-ss] "
        InitLogFile(true)
    }
    func error(_ message:String){
        date = Date()
        let result = formatter.string(from: date)
        logger.error("\(result)\(message)")
    }
    func debug(_ message: String){
        date = Date()
        let result = formatter.string(from: date)
        logger.debug("\(result)\(message)")
    }
}
